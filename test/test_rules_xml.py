import unittest
import pathlib
import lxml.etree as etree
import pandas
import logging
import re

from src.writer import Writer
from src.parse_xml import ParseV4, ParseV2, ParseV1, ParseHistoric


class USPTOLogger(logging.Logger):

    def _log(self, level, msg, args, **kwargs):
        if 'pat_nbr' in kwargs:
            extra = {'pat_nbr': kwargs.get('pat_nbr')}
        else:
            extra = {'pat_nbr': '0'}
        super(USPTOLogger, self)._log(level, msg, args, kwargs.get('exc_info', None), extra)
logging.setLoggerClass(USPTOLogger)
logging.basicConfig(format='[[{levelname} - {name}:{pat_nbr}:{msg}]]', style='{',
                    level=logging.WARNING)


class TestRulesXML(unittest.TestCase):

    names = ['pat_nbr', 'te', 'teg', 'chem', 'chemg', 'cid', 'seq', 'lvl', 'dep', 'text']

    def setUp(self):
        self.data_path = pathlib.Path('../test_data/patents/')
        self.out_file = pathlib.Path('../test_data/test/tmp.csv')
        self.out_path = pathlib.Path('../test_data/')

        self.writer = Writer(self.out_path)

    def process_patent(self, pat: pathlib.Path, parser, claims_xpath):
        """
        This method reads in a single patent, parses the dependency
        and compares the results with the stored results.py

        It does not provide a fancy handler for the output, but simply
        starts a new file for each patent, reads in the file and delets it afterwards.
        """
        self.writer.new_file('test', 'tmp.csv')

        with pat.open('rb') as ifile:
            patent = etree.parse(ifile).getroot()
        claims = patent.xpath(claims_xpath)[0]
        tmp = [(e.get('seq'), e.get('dep'), e.get('cid')) for e in
                   patent.xpath('/patent/results/line')]
        good = {'seq': [x[0] for x in tmp], 'dep': [x[1] for x in tmp],
                'cid': [x[2] for x in tmp]}
        good = pandas.DataFrame.from_dict(good)

        parser.parse_claims(claims, patent.get('pat_nbr'))
        # close the file of the writer, so that I can savely read it in again.
        self.writer._file.close()
        self.writer._file = None
        parsed = pandas.read_csv(self.out_file, names=self.names,
                                 dtype=dict([(x, str) for x in self.names]))
        parsed = parsed.filter(['seq', 'dep', 'cid'])

        # And finally compare them. I want to do so in a single statement.
        pandas.testing.assert_frame_equal(good, parsed)
        self.out_file.unlink()

    def test_patents_V4(self):
        parser = ParseV4(self.writer, False)
        reg_v4 = re.compile(r'(RE)?[0-9]*.xml')
        patents = [f for f in self.data_path.glob('*.xml') if reg_v4.match(f.name) is not None]

        for patent in patents:
            print(patent.name)
            self.process_patent(patent, parser, '/patent/claims/claims')

    def test_applications_V4(self):
        parser = ParseV4(self.writer, True)
        reg_v4 = re.compile(r'(RE)?[0-9]*_app.xml')
        patents = [f for f in self.data_path.glob('*.xml') if reg_v4.match(f.name) is not None]

        for patent in patents:
            print(patent.name)
            self.process_patent(patent, parser, '/patent/claims/claims')

    def test_patents_V2(self):
        parser = ParseV2(self.writer, False)
        reg_v4 = re.compile(r'(RE)?[0-9]*_v2.xml')
        patents = [f for f in self.data_path.glob('*.xml') if reg_v4.match(f.name) is not None]

        for patent in patents:
            print(patent.name)
            self.process_patent(patent, parser, '/patent/claims/CL')

    def test_applications_V1(self):
        parser = ParseV1(self.writer, True)
        reg_appv1 = re.compile(r'^[0-9]*_app_v1.xml')
        patents = [f for f in self.data_path.glob('*.xml') if reg_appv1.match(f.name) is not None]

        for patent in patents:
            print(patent.name)
            self.process_patent(patent, parser, '/patent/claims/subdoc-claims')

    def test_patents_historic(self):
        parser = ParseHistoric(self.writer)
        reg_hist = re.compile(r'^US-[0-9]*-A.xml')
        patents = [f for f in self.data_path.glob('*.xml') if reg_hist.match(f.name) is not None]
        
        for patent in patents:
            print(patent.name)
            self.process_patent(patent, parser, '/patent/claims/div')

    def tearDown(self):
        if self.out_file.exists():
            self.out_file.unlink()
        (self.out_path / 'test').rmdir()


if __name__ == '__main__':
    unittest.main()
