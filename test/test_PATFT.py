import unittest
import pickle

import PyFWParser as fwparser


class TestPATFT(unittest.TestCase):

    def setUp(self) -> None:
        fwparser.doc_bound('*** BRS DOCUMENT BOUNDARY ***')
        fwparser.open_file('../test_data/patft1.txt')

        with open('../test_data/patft1.pickle', 'rb') as ifile:
            self.patft1 = pickle.load(ifile)
        self.keys = ['WKU', 'DWKU', 'ABPR']
        self.key_data = [i for i in self.patft1 if i[0] in self.keys]

    def test_patft(self):
        self.assertEqual(self.patft1, fwparser.get(), 'python full')

        fwparser.set_keys(self.keys)
        self.assertEqual(self.key_data, fwparser.get(), 'python keys')

        self.assertIsNone(fwparser.get(), 'python last')

    def tearDown(self) -> None:
        fwparser.reset()


if __name__ == '__main__':
    unittest.main()
