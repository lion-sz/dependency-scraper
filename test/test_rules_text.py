import unittest
import pathlib
import json
import logging

from src.rules import apply_rules


class USPTOLogger(logging.Logger):

    def _log(self, level, msg, args, **kwargs):
        if 'pat_nbr' in kwargs:
            extra = {'pat_nbr': kwargs.get('pat_nbr')}
        else:
            extra = {'pat_nbr': '0'}
        super(USPTOLogger, self)._log(level, msg, args, kwargs.get('exc_info', None), extra)
logging.setLoggerClass(USPTOLogger)
logging.basicConfig(format='[[{levelname} - {name}:{pat_nbr}:{msg}]]', style='{',
                    level=logging.WARNING)

class TestRules(unittest.TestCase):

    # Testing Rule 1 is not directly possible.
    # Similarly, rule 2 does not require explicit tests.
    # Rule 3 - 6: Keywords for finding independent and dependent claims in
    # lvl 1 claims.
    r3t6 = [
        (1, [(1, 'an empty speech bubble.')]),
        (2, [(1, 'the speech bubble recited in claim 1, colored red.')]),
        (3, [(1, 'the speech bubble claimed in 1, colored in green.')]),
        (4, [(1, 'the speech bubble recited in 1, colored in pink.')]),
        (5, [(1, 'an speech bubble filled with nonsense.')]),
        (6, [(1, 'the speech bubble recited in claim 5, colored red.')]),
        (7, [(1, 'the speech bubble claimed in 5, colored in green.')]),
        (8, [(1, 'the speech bubble recited in 5, colored in pink.')]),
    ]
    r3t6_res = [0, 1, 1, 1, 0, 5, 5, 5]

    def setUp(self) -> None:
        self.logger = logging.getLogger('TestRules')

    def run_test(self, claims, expected):
        seq = 1
        parsed = {}
        for id, claim in claims:
            parsed[str(id)] = []
            for lvl, text in claim:
                dep = apply_rules(self.logger, {}, None, text, lvl, seq, id, 'test', parsed)
                self.assertEqual(expected[seq - 1], dep, 'line number {}'.format(seq))

    def test_r3t6(self):
        self.run_test(self.r3t6, self.r3t6_res)

    def process(self, pat: pathlib.Path) -> None:
        with pat.open('r') as ifile:
            patent = json.load(ifile)

        parsed_claims = {}
        last_id = 0
        for line in patent:
            if line['id'] != last_id:
                parsed_claims[str(line['id'])] = []
                last_id = line['id']
            dep = apply_rules(self.logger, {}, None, line['text'], line['lvl'], line['seq'],
                              line['id'], 'test', parsed_claims)
            self.assertEqual(dep, line['dep'], f'Found when checking claim {line["id"]}')

    def test_good_text_pats(self):
        pat_dir = pathlib.Path('../test_data/patents/')
        pats = list(pat_dir.glob('*.json'))

        for pat in pats:
            print(pat.name.replace('.json', ''))
            self.process(pat)



if __name__ == '__main__':
    unittest.main()
