import unittest
import logging
import pickle

logger = logging.getLogger()
logger.level = logging.DEBUG

from src.parse_text import ParseAPS, ParsePATFT

class MyTestCase(unittest.TestCase):

    # Well A well formatted patent with no subpars
    patft1 = [
        ('CLMS', None),
        ('CLNO', '1.'), ('CLPR', '1. Sample Text.'),
        ('CLNO', '2.'), ('CLPR', '2. The text from claim 1 with further stuff.'),
        ('CLNO', '3.'), ('CLPR', '3. The thing in claim 2 with something red on it.')
    ]
    patft1_res = [
        (1, False, [(1, '1. Sample Text.')]),
        (2, False, [(1, '2. The text from claim 1 with further stuff.')]),
        (3, False, [(1, '3. The thing in claim 2 with something red on it.')])
    ]

    # multiple pars, lvl 2, but PATFT cannot store lvl 3
    patft2 = [
        ('CLMS', None),
        ('CLNO', '1.'), ('CLPR', '1. A sample text comprising of:'),
        ('CLPR', 'A great nothing'), ('CLPR', 'and an empty speech bubble.'),
        ('CLNO', '2.'), ('CLPR', '2. The nothing of claim 1, further comprising of:'),
        ('CLPR', 'An empty space, filled with either:'),
        ('CLPR', 'Nothing, or'), ('CLPR', 'a lot of hot air.')
    ]
    patft2_res = [
        (1, False, [(1, '1. A sample text comprising of:'),
             (2, 'A great nothing'), (2, 'and an empty speech bubble.')]),
        (2, False, [(1, '2. The nothing of claim 1, further comprising of:'),
             (2, 'An empty space, filled with either:'),
             (2, 'Nothing, or'), (2, 'a lot of hot air.')])
    ]

    # a mislabeled claim
    patft3 = [
        ('CLMS', None),
        ('CLNO', '1.'), ('CLPR', '1. A sample text comprising of:'),
        ('CLPR', 'A great nothing'), ('CLPR', 'and an empty speech bubble.'),
        ('CLNO', '3.'), ('CLPR', '3. The nothing of claim 1, further comprising of:'),
        ('CLPR', 'An empty space, filled with either:'),
        ('CLPR', 'Nothing, or'), ('CLPR', 'a lot of hot air.')
    ]
    patft3_res = [
        (1, False, [(1, '1. A sample text comprising of:'),
             (2, 'A great nothing'), (2, 'and an empty speech bubble.')]),
        (2, False, [(1, '3. The nothing of claim 1, further comprising of:'),
             (2, 'An empty space, filled with either:'),
             (2, 'Nothing, or'), (2, 'a lot of hot air.')])
    ]

    # No subpars, simple patent.
    aps1 = [
        ('CLMS', None),
        ('NUM', '1.'), ('PAR', '1. Sample text.'),
        ('NUM', '2.'), ('PAR', '2. The text from claim 1 with further stuff.'),
        ('NUM', '3.'), ('PAR', '3. The thing in claim 2 with something red on it.')
    ]
    aps1_res = [
        (1, False, [(1, '1. Sample text.')]),
        (2, False, [(1, '2. The text from claim 1 with further stuff.')]),
        (3, False, [(1, '3. The thing in claim 2 with something red on it.')])
    ]
    # Simple level 2 paragraphs
    aps2 = [
        ('CLMS', None),
        ('NUM', '1.'), ('PAR', '1. A sample text comprising of:'),
        ('PA1', 'A great nothing'), ('PA1', 'and an empty speech bubble.'),
        ('NUM', '2.'), ('PAR', '2. The nothing of claim 1, further comprising of:'),
        ('PA1', 'An empty space, filled with either:'),
        ('PA2', 'Nothing, or'), ('PA2', 'a lot of hot air.')
    ]
    aps2_res = [
        (1, False, [(1, '1. A sample text comprising of:'),
             (2, 'A great nothing'), (2, 'and an empty speech bubble.')]),
        (2, False, [(1, '2. The nothing of claim 1, further comprising of:'),
             (2, 'An empty space, filled with either:'),
             (3, 'Nothing, or'), (3, 'a lot of hot air.')])
    ]

    def test_basic_structure_PATFT(self):
        msg = lambda x: 'PATFT text, C parser: {}'.format(x)
        parser: ParsePATFT = ParsePATFT(None, False)

        self.assertEqual(self.patft1_res, parser._build_claim_struct(self.patft1, 'test'),
                         msg('simple example'))
        self.assertEqual(self.patft2_res, parser._build_claim_struct(self.patft2, 'test'),
                         msg('lvl 2 example'))

    #def test_warning_structure_PATFT(self):
    #    msg = lambda x: 'PATFT text, C parser: {}'.format(x)
    #    parser: ParsePATFT = ParsePATFT(None, False)

    #    with self.assertLogs(level='WARNING') as cm:
    #        self.assertEqual(self.patft3_res, parser._build_claim_struct(self.patft3, 'test'), msg('bad numbering'))

    #    self.assertEqual(cm.output, ['WARNING:ParsePATFT:claim number differ: 3 parsed, but counted 2'])

    def test_basic_structure_APS(self):
        msg = lambda x: 'APS text, C parser: {}'.format(x)
        parser: ParseAPS = ParseAPS(None, False)

        self.assertEqual(self.aps1_res, parser._build_claim_struct(self.aps1, 'test'),
                         msg('simple example'))
        self.assertEqual(self.aps2_res, parser._build_claim_struct(self.aps2, 'test'),
                         msg('lvl2 structure'))

if __name__ == '__main__':
    unittest.main()
