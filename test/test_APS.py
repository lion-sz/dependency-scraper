import unittest
import pickle

import PyFWParser as fwparser


class TestAPS(unittest.TestCase):

    def setUp(self) -> None:
        fwparser.doc_bound('PATN')
        fwparser.open_file('../test_data/aps1.txt', 2)

        with open('../test_data/aps1.pickle', 'rb') as ifile:
            self.aps1 = pickle.load(ifile)
        self.keys = ['WKU', 'DWKU', 'ABPR']
        self.key_data = [i for i in self.aps1 if i[0] in self.keys]

    def test_aps(self):
        self.assertEqual(self.aps1, fwparser.get(), 'python full run')

        fwparser.set_keys(self.keys)
        self.assertEqual(self.key_data, fwparser.get(), 'python keywords')

        self.assertIsNone(fwparser.get(), 'python last entry')

    def tearDown(self) -> None:
        fwparser.reset()

if __name__ == '__main__':
    unittest.main()
