#!/usr/bin/python3

import traceback
import logging
import datetime
import sys
import pathlib

import src.config_reader
from src import bulkdata_parser
from src import interface_parser


class USPTOLogger(logging.Logger):
    def _log(self, level, msg, args, **kwargs):
        if "pat_nbr" in kwargs:
            extra = {"pat_nbr": kwargs.get("pat_nbr")}
        else:
            extra = {"pat_nbr": "0"}
        if kwargs.get("exc_info") is not None:
            msg = msg + "\n" + traceback.format_exc()
        super(USPTOLogger, self)._log(level, msg, args, None, extra)


logging.setLoggerClass(USPTOLogger)


def __main__():
    """
    It first sets up the arguments, e.g. expands years and asks if paths are
    correct. If quiet is set there is no output.
    Then the parser classes are initialized and finally in the main loop the parsing happens.
    """

    # preparing arguments
    parser = src.config_reader.Config_Reader("uspto.ini")
    args = parser.read_conf()

    # For all paths check if there is a leading slash. If not add it.
    paths = ["path", "out_path", "tmp_path"]
    for path in paths:
        if args[path] is not None:
            if args[path][-1] != "/":
                args[path] += "/"

    # get the quiet argument
    quiet = args["quiet"]

    logging.basicConfig(
        filename="uspto.log",
        filemode="w",
        level=logging.WARNING,
        format="[[{levelname} - {name}:{pat_nbr}:{msg}]]",
        style="{",
    )
    logger = logging.getLogger("uspto")

    # check for update mode.
    # if set perform some further checks
    if args["update"]:
        if args["no_offline"]:
            logger.critical("No use of offline files not permitted in update mode")
            sys.exit()
        if args["year"] is None:
            now = datetime.datetime.now()
            years = [now.year]
        else:
            years = [args["year"]]
    else:
        years = list(range(args["start_year"], args["end_year"] + 1))
        if type(args["year"]) is int:
            years = [args["year"]]

    # build class objects
    bulk = bulkdata_parser.Bulk(
        allow_offline=not args["no_offline"],
        data_path=args["path"],
        tmp_path=args["tmp_path"],
        expand_path=not args["no_expand"],
        update=args["update"],
        applications=args["applications"],
        force_reload=False,
    )

    interface = interface_parser.Parser(args["out_path"], args["applications"])

    # Check, if I'm working on historic patents.
    if "historic" in args and args["historic"] is not None:
        hist_path = pathlib.Path(args["historic"])
        zip_path = pathlib.Path(args["historic"].replace("json", "zip"))
        interface.parse("historic", zip_path, hist_path, historic=True)
        return

    # Check if I'm working with canadian patents.
    if "canada" in args and args["canada"] is not None:
        path = pathlib.Path(args["canada"])
        interface.parse(
            "canada",
            path,
            path,
            historic=False,
            canadian=True,
        )
        return

    # main loop
    for year in years:
        bulk.open_year(year)
        for zip_name, xml_path in bulk:
            if zip_name is None:
                logger.critical("Zipfile is None: {}".format(zip_name))
                continue
            if not quiet:
                print(zip_name)
            logger.info("opening zipfile {}".format(zip_name))
            interface.parse(str(year), zip_name, xml_path)
            bulk.cleanup(zip_name, xml_path)


if __name__ == "__main__":
    __main__()
