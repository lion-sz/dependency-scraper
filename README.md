A longer documentation including the rules used and which corrections I apply, can be found at [lion-sz.gitlab.io/dependency-scraper](https://lion-sz.gitlab.io/dependency-scraper/).

# Purpose

This script is supposed to parse the patents published by the us patent and trade office and extract from it the claims and their dependency structure.
 
It runs on both the text files published between 1976 and 2002 and the xml files published after that.

It ignores among others design patents, since these usually do not feature any text describing the patented object.

# Comma Bug

My Code before 2021 contains a bug that causes me to miss references, especially preceding claims.
This is caused by me checking for preceding claims only if I've previously found a spelling used to identify claims.
These spellings expect a whitespace after the word, which means I miss references such as `any one of the preceding claims,`.

# output

This code writes one output file for every input file, with the exact same name replacing only the filetype.
The .csv files are separated with a comma while strings are escaped with '"' characters.

There is no header, the names of the columns are:

 * 'PatentID'
 * 'TE': does this line contain a table or equation?
 * 'TE_global': does this patent contain a table or equation?
 * 'CHEM': does this line contain a chemical formula?
 * 'CHEM_global': does this patent contain a chemical formula?
 * 'claim_id': reported claim id
 * 'sequence': sequence number of the lines
 * 'level'
 * 'dependency'
 * 'text'

**Independend claims have the dependency 0.** 

## Mathematical and chemical formulas in text

The parsing of mathematical and chemical formulas is not implemented yet, especially in the xml files. When the parser encounters them it simply casts everything to text.
This can seriously distort the text, especially since all tags are ignored.

Additionally sometimes, when the line contains a '=' sign, the parser fails to include the text after this sign.


# Rules

The script implements the ruleset 'Splitter Rules' from July 23, 2016.
Only slight alterations are made.
These are listed below.

## Alteration rule 9

I've altered rule 9.
Claims that reference several preceding patents no longer are assigned the first claim as dependency, but the previous one.

## Preceding claims, specifying numbers.

Some claims refer to e.g. `a method according to any of preceding claims 3-4`.
In this case the rules 3,4,6,7 (direct reference to a claim) take precedence,
and I take as dependence the lowest of the directly referenced claims.

## Altered handling of failed parsing

If I've found some reference to claims, therefore if any of the keywords in spellings_claim are found, but no other parsing had any luck I now assign as dependency the last claim.

## No more nonexistent dependencies

By altering the handling of failed parsing I've no eliminated the only case that produced dependency -1.
My code therefore assigns a dependency greater or equal to 0 to every claim with 0 indicating an independent claim.

## omitting some patents

Some patents are omitted. All patents that have 'D', 'P', 'H', 'T' as the first char in the patent number are not interesting and are ignored.

This is done by the Parser class, which simply continues for design patents.

## warning: empty line

This warning is caused by a different carriage return in tables. There the windows two byte version is used which is ^M and then a "normal" newline.
Python reads that as two new lines and therefore this causes a warning to be logged.
Note that newer versions of the scraper only show a debug message, since this is expected and unproblematic behaviour.




# lengthy documentation
 
This documentation requires the use of a bash terminal.
Under Windows Powershell is required.
For MacOS the default terminal - and therefore zsh - should be fine, but were not tested.
The code was developed under CentOS and Arch Linux, but it should work independent of the operating system.


## Setup to run the scraper 

Running this scraper requires python 3.7 or newer installed on your machine.
Older versions of python 3 are not tested but should work.
Note that the 64 bit version of python is recommended and on Windows it is required.
If the script fails - especially for later years - with a MemoryError this indicates that the installed version of python is 32 bit and therefore cannot use sufficient amounts of memory.

The setup of python is slightly different across operating systems, but there are plenty good online resources.
In the following I'll assume that you can run python through the command `python`.

Furthermore, there are three additional packages required:

 * lxml
 * bs4
 * csv

To check if you have a package installed simply try importing it with 'import'.
To install a python package use the python program pip that should come with your python installation:
```bash
pip install $package_name
```
where you replace `$package_name` with the actual package name. To work this requires that the terminal is started as administrator under Windows, under Linux run the command as root. 

For this script to work **all** files must be in the same path! The easiest way to achieve this is to simply download this repository either with:
```bash
git clone https://gitlab.com/ClF3/uspto.git
```
or download it as a .zip file and unpack it in the target directory.


## Simplified running with 'run.sh'

The file 'run.sh' is a bash script that allows for a simpler process.
It automates running the python script for the specified years and after every year collects the result files and zips them to a singe zip file named after the year.

In order to use this you have to make small alterations to the script itself.
The relevant file paths and the years must be specified in the source itself:
 * 'data\_path': The folder in which the data files are. This script assumes the annually subfolders to be present.
 * 'output\_path': A temporary folder to store the output files.
 * 'tmp\_path': A temporary folder to store the unzipped input files.
 * 'target\_path': A folder where the zipped output files should be moved to.
 * 'logpath': A folder to store the log files.
Note that the two folder 'output\_path' and 'tmp\_path' can be removed once the script is complete.
And since these two folders receive the most read / write access these should **not** be network drives.
Otherwise the script would be significantly slower.
The data and target folders on the other hand can be network drives, since they are written to or read from less and are only for storing results / input data.

The years can be specified in line 9: 'for year in $seq(start end)'.
Instead of 'start' insert the first year that you want to run the code for and for 'end' the last year.
Note that the code will run for the year 'end', since this is included in the sequence generated!


## Running the python script

In order to run this script open your preferred terminal, navigate to the folder with the file `uspto.py` and run the command:
```bash
python uspto.py $command_line_options
```
where the `$command_line_options`specify what the script will do.



## default behaviour

When run with defaults, therefor no command line options this script goes through the predefined years and for every year it searches for the weekly embodiment publications in .zip format.
By default it only searches the current directory assuming that the files are in folders named `$year` + `_xml` for newer and `$year` for older files.
When the file is found it will extract the xml file, parse it, write the output in the same folder as the input file and delete the xml file, leaving only the zip archive and the output file.

When the zip archive for the week is not found it will go ahead and download it, then perform the same action as above.

When starting to parse the file the script outputs the filename it is working on. This can be seen as a status - if the script is unexpectedly slow, e.g. it takes a minute before starting the first file this might indicate it does not see the already downloaded files.
Recheck the path command line option!

**This script requires an internet connection even for downloaded files!**


## command line options

The most important options are help, path and year.
The other are for more specialised cases.

### help

Run the script with just `--help` and you will get a short help text.

### year

This option can be specified by setting `--year 2020` or `-y 2020`. It specifies for which year the code should run.
It can only take single years. If it should run for several years alter the list `years` in the source code.

### path

This options specifies the path to use. If e.g. the already downloaded files are under `C:/Users/lion/USPTO/files` this has to be specified using the flag `--path C:/Users/lion/USPTO/files` or `-p C:/Users/lion/USPTO/files`.

### update

When the flag `--update` or `-u` is set the script will work in update mode.
It will not parse old files, but download and parse files no already in the directory.

### offline

This option does not require an argument.
It can be set by `--offline` or `-o`.
It specifies whether offline files should be used.
When the flag is set the code does not search for offline files but rather downloads all files.
This will significantly increase runtime, but also reduce the required storage.
By default, when the flag is not set, the script tries to use offline files, which is recommended.


### expand

This flag does not require an argument.
It can be set by `--expand` or `-e`.
It specifies if the path for the datafiles should be expanded, therefore if the folders `$year` + `_xml` for xml files and `$year` for text files should be used.
When omitting this flag the folders are used, which is recommended.



## update mode

The update mode is meant to run periodically, e.g. every few weeks and check for new patent files.
These are then downloaded and parsed.
Therefore, the script will skip all already existing files.
It should be run with the path for the old files or the script will end up downloading all files again!
If no year is provided it will use the current year.
The update mode cannot run together with the offline flag.



## system requirements

The script is not very well optimized. It can only use 1 cpu core at a time and does not benefit from a high powered computer.
But since It loads large files into memory and writes a lot of small entries to a file it will fail without at least 2 GB of available memory and certainly benefits from a SSD.

Therefore, I recommend:
- 2 core Cpu
- 4 GB of memory for text files, 8 GB of memory for xml files
- as much free storage as for the zip files that should be processed.

When Storage is a problem the script can be run year by year, e.g. using a bash script to manage zip the result files after each year is complete.



## bugs or questions?

Please add bugs or questions either to this gitlab page or write an email to <lion-sz@gmx.de>.
