from typing import List, Tuple, Optional


file = None
file_readable: bool = False

doc_boundary = None
doc_bound_len = 0

relevant_keys = None
section = None

def open_file(path: str, remove_lines: int=1) -> None:
    """
    This function opens a new file.
    Since the files might have leading lines (including a document boundary string),
    you can further specify the number of lines to remove from the head of the document.

    Note that you must set the documnt boundary string (see :func:`~PyFWParser.doc_bound`) before calling this function.

    The file is opened as an utf-8 file and all encoding errors are replaced.
    Therefore, if there are any `?` characters in strange places, these are the results of encoding errors.

    :param path: The filepath that should be opened
    :type path: str
    :param remove_lines: How many lines should be removed from the top of the file.
    :type remove_lines: int, optional
    :return: None
    :raises RuntimeError: If no document boundary is set, reading the arguments or opening the file failed.
    """
    global file
    global file_readable
    if doc_bound is None:
        raise RuntimeError('Please set the document boundary before opening a file!')

    if type(path) is not str:
        raise RuntimeError('Reading arguments failed, arguments not string?')

    try:
        file = open(path, 'r', encoding='utf-8', errors='replace')
    except Exception:
        raise RuntimeError('File opening failed, missing file?')

    for i in range(remove_lines):
        try:
            next(file)
        except StopIteration:
            raise RuntimeError('Empty file found!')
    file_readable = True
    return None

def close_file() -> None:
    """
    Close an opened file.

    :return: None
    """
    global file
    global file_readable
    if file is not None:
        file.close()
        file = None
        file_readable = False


def doc_bound(boundary: str) -> None:
    """
    This function is used to set the line that separates the entries.
    In the PATFT format this is *\*\*\* BRS DOCUMENT BOUNDARY ]*\*\**,
    and in the APS format *PATN*.
    To ensure that the document boundary is set, opening a file will fail, untill this function
    was called.

    :param boundary: The line used to separate entries
    :param boundary: str
    :return: None
    :raises RuntimeError: If the argument is not a string.
    """
    global doc_boundary
    global doc_bound_len
    if type(boundary) is not str:
        raise RuntimeError('Argument must be string.')
    doc_boundary = boundary
    doc_bound_len = len(doc_boundary)
    return None


def set_keys(keys: List[str]) -> None:
    """
    When looking for only a few fields in a patent publication, this option allows you to specify a list of
    relevant keywords. If set, only these keywords will be returned from any call to :func:`~PyFWParser.get`.

    To reset the keys simply call the this function with None as argument.

    :param keys: The keys that will be set.
    :type keys: List[str] or None
    :return: None
    :raises TypeError: If the parameter is not a list of strings or None.
    """
    global relevant_keys
    if keys is None:
        relevant_keys = keys
    if type(keys) is not list:
        raise TypeError('parameter must be list,')
    for key in keys:
        if type(key) is not str:
            raise TypeError('Argument must be a list of strings!')
    relevant_keys = keys

def set_sections(sec: List[str]) -> None:
    """
    As an alternative to the keys, you can specify which sections to include in the output.
    A section is started by a key with an empty value.
    Right now I do not stop when encountering a new empty key, as this might
    stop the parsing of a malformed patent.

    If None is provided, the current sections are reset.

    :param sec: The sections that should be included.
    :type sec: List[Str] or None
    :return: None
    :raises TypeError: If the argument ist not a list of strings or None.
    """
    global section
    if sec is None:
        section = None
    if type(sec) != list:
        raise TypeError('Argument must be a list of strings!')
    for key in sec:
        if type(key) != str:
            raise TypeError('Argument must be a list of strings!')
    section = sec

def get() -> Optional[List[Tuple[str, str]]]:
    """
    This function reads in and returns a patent.
    The patent is returned as a list of tuples, each containing the key and the text.
    It removes whitespaces from the key and correctly formats multiline fields.
    If it encounters an empty patent or the file ends, None is returned instead.

    :return: The patent, formatted as a list or None, when the file neds.
    :rtype: Optional[List[Tuple[str, str]]]
    :raises RuntimeError: If the file is not jet initialized.
    """
    global file_readable
    if file is None:
        raise RuntimeError('File not initialized.')
    if not file_readable:
        return None

    entry = []
    key = ''
    val = ''
    skip_key = False
    include_section = False
    while True:
        try:
            line = next(file)
        except StopIteration:
            file_readable = False
            if key != '':
                entry.append((key, val if val != '' else None))
            break
        line.replace('\n', '')

        # continuation keys are either skipped or appended.
        if line[0:4] == '    ':
            if not skip_key or include_section:
                val += line[5:].rstrip()
            continue

        # New key - append last line, reset and check relevance.
        if key != '':
            entry.append((key, val if val != '' else None))
            key = ''
            val = ''

        if len(line) >= doc_bound_len and line[0:doc_bound_len] == doc_boundary:
            break

        if relevant_keys is not None:
            skip_key = line[0:4].rstrip() not in relevant_keys

        tmp_key = line[0:4].rstrip()
        tmp_val = line[5:].rstrip()
        if section is not None:
            if not include_section and tmp_val == '':
                include_section = tmp_key in section

        if (not skip_key) or include_section:
            key = tmp_key
            val = tmp_val

    return entry if entry != [] else None

def reset() -> None:
    """
    This function simply resets all internal fields.
    It should be called, when one parser no longer uses the module, but another might do so later.
    Right now it is the only way to reset the relevant_keys.

    :return: None
    """
    global relevant_keys
    global doc_boundary
    global doc_bound_len
    global section

    close_file()
    relevant_keys = None
    doc_boundary = None
    doc_bound_len = 0
    section = None

    return None
