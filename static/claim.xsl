<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="html"/>

    <xsl:template match="/patent">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>US Patent <xsl:value-of select="@pat_nbr"/></title>
            <link rel="stylesheet" type="text/css" href="static/claim.css" />
        </head>
    <body>

        <h2>Patent <xsl:value-of select="@pat_nbr"/></h2>
        <p>Published <xsl:value-of select="@date"/></p>

        <xsl:apply-templates select="claims|subdoc-claims"/>

    </body>
    </html>
    </xsl:template>

    <xsl:template match="claims|subdoc-claims">
        <xsl:for-each select="claim">
            <div class="claim">
                <h4>Claim <xsl:value-of select="@num|@id"/></h4>
                <xsl:apply-templates select="claim-text"/>
            </div>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="claim-text">
        <div class="claim-text">
            <xsl:for-each select="child::node()">
                <xsl:choose>
                    <xsl:when test="name() = 'claim-text'">
                        <xsl:apply-templates select="."/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="." mode="serialize"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template match="claim-ref|dependent-claim-reference" mode="serialize">
        <b class="claim-ref"><xsl:value-of select="text()"/></b>
    </xsl:template>

    <xsl:template match="highlight" mode="serialize"> <!-- remove this tag, it is only used in v1 -->
        <xsl:apply-templates mode="serialize"/>
    </xsl:template>
    <xsl:template match="b|bold" mode="serialize">
        <b><xsl:value-of select="text()"/></b>
    </xsl:template>
    <xsl:template match="sub|subscript" mode="serialize">
        <sub><xsl:value-of select="text()"/></sub>
    </xsl:template>
    <xsl:template match="sup|superscript" mode="serialize">
        <sup><xsl:value-of select="text()"/></sup>
    </xsl:template>

    <xsl:template match="*" mode="serialize">
        <xsl:text>&lt;</xsl:text>
        <xsl:value-of select="name()"/>
        <xsl:text>&gt;</xsl:text>
        <xsl:apply-templates mode="serialize"/>
        <xsl:text>&lt;/</xsl:text>
        <xsl:value-of select="name()"/>
        <xsl:text>&gt;</xsl:text>
    </xsl:template>

</xsl:stylesheet>