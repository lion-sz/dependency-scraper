# Problems

In this file I want to document problems I found while creating the documentation.

- [x] parse text: When I find several claim keywords I return a list of claim keywords. This behaviour seems problematic and like a bug.
    Therefor I edited it. See alterations.


# Alterations

Here I want to document alterations to the code that I made while writing the documentation.

- The writer now has its own module.
- When several claim keywords are found the text parser now returns None None after logging the error.
