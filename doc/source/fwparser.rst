Fixed Width Parser
==================

To flexibly parse the fixed width text files (PATFT and APS), I've written a small python module that parses these files and returns a list of key, value pairs that represent the file.

Furthermore, it allows for two ways to filter the output:

* Relevant Keys: If these are set, only the keys specified here are returned. This is handy, when e.g. only a few fields of metadata are wanted.
* Section: A section is started by a key with an empty value (e.g. CLMS starts the claims section). When a section (or a list of sections) was specified the parser will return all keys, that follow this section key. Note, that as of now a section is never ended.

These two methods can be combined, in this case a key is returned if either of the conditions is true.


PyFWParser
-----------

.. automodule:: PyFWParser
    :members: open_file, close_file, doc_bound, set_sections, set_keys, get, reset