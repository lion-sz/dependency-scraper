=============
Usage
=============

Requirements
============

This script is developed and tested under Linux.
It is tested with python 3.9, but older versions of python should work as well.
It requires at least 4GB system memory tough I recommend 8GB to ensure that even later years run without problems.
Since the Bulkdata Parser downloads the list of all files for one year the script requires a constant internet connection, even if local files are used.

The run script requires a bash terminal while the python script should run independently of the operating system.

The following python packages that do not come with the python installation are required:

* `bs4 <https://www.crummy.com/software/BeautifulSoup/>`_
* `lxml <https://lxml.de>`_

These packages can be installed through the python package manager `pip <https://pypi.org/project/pip/>`_.



Running
=============

There are two different ways to run this script.
Directly calling the python script or using the bash script `run.sh`, which zips the output files into yearly archives.

Configuration can be done through the :ref:`configuration`.



Basic usage: run.sh
--------------------

This script is written in bash and intended for use on a Linux machine.
Therefore it requires programs like zip, mv, rm, seq and will probably not run on Windows machines.
I've not tested the script for MacOs but it might work.

Before running the script you should set the paths of the input and output folders.
This has to be done by editing the run.sh file directly.
There are 5 important variables in the file:

* data_path: The path where the input files are.
* target_path: The path where the final zipped annual files should be placed.
* tmp_path: After unzipping the input files these are stored in a temporary path.
* output_path: A temporary path where the output of the script can be stored before moved to the target_path.
* log_path: A place to store the log files.

Since the script will write a lot of temporary data to the tmp_path and the output_path these should not be network drives.
The data and the target paths however can be network drives.

To run this script navigate to the folder with the code and call the script.
You must provide both a start year and an end year, for example in order to parse claims between 2006 and 2020 run:

.. code-block:: bash

    ./run.sh 2006 2020


Update mode:
------------

The run script also has an update mode.
In this mode the script runs for the current year and only processes files that are not yet downloaded.
Therefore it needs to find the already downloaded files or it might end up downloading everything again.

To use the update mode first customize the five file paths above and then run the script as follows:

.. code-block:: bash

    ./run.sh update


Direct usage: uspto.py
----------------------

If finer control over the script is needed you can start it directly in python.
To do so start a terminal, navigate to the folder in which the file `uspto.py` is and start the script by running:

.. code-block:: bash

    python uspto.py

When doing so you have to provide the script with additional information, such as the path for the input files or the years it should parse.
This can be done in one of two ways; you can give the arguments when calling the python script by specifying one ore more of the named arguments listed below or you can add the desired argument to the :ref:`configuration`.

The mayor exception are the years.
If the script should run only one year this must be given as a command line argument.
If data for several years should be scraped these years can be given as arguments.
They can be specified through the `start` and `end` command line flags or the options in the configuration file.

When you expect to use the script often, e.g. for monthly updates, consider storing the arguments provided in the config file.
This can be done using the `--store` flag.

.. argparse::
    :module: src.config_reader
    :func: build_parser


.. _configuration:

Config File
==============

The uspto script can be configured by either the command line options or the configuration file.
When provided the command line options take precedence over the options provided in the configuration file.
The config file is primarily a way to make the command line options permanent.

At every start the script looks for this file and if the file is found it reads in the options.
If the file is not found the script will use the default for all options that are not specified through the command line.
If single options are missing from the file (e.g. because you only wanted to specify the paths) the defaults are assumed for all other options.

The file structure used is a simple ini file.
It is organized in sections and every section contains keys.
For a quick example on how this looks see the `supported INI file structure <https://docs.python.org/3/library/configparser.html#supported-ini-file-structure>`_.

The three sections supported are:

paths
-----

The section paths specifies the paths used in the script.
There are three keys in this section.

.. option:: path

    :type: str
    :default: ./

    The input path where the data files are stored.
    This can be an external drive or a network drive since (if the temporary path is set) this folder is not written to and only the zipped files are read.

.. option:: out_path

    :type: str
    :default: ./

    The path where the output files are placed.
    This should be a local folder since the script will write many small chunks of uncompressed data to the files.

.. option:: tmp_path

    :type: str
    :default: None

    A temporary path that is used to store the deflated input files.
    If set to None the same path where the input files are placed is used
    to store the deflated files.

    When the input path is a network drive or a slow external drive this option can significantly reduce overhead by using a faster drive for the (rather large) extracted files.


flags
-----

This section contains the four flags.
Since these flags are true false values you can set them trough 'True', 'true', 'yes', '1' or 'on' if you want to use the flag or 'False', 'false', 'no', '0' or 'off' if you do not want to use these flags.

When a flag is set in the config file the command line cannot unset this flag!
If you want to remove a flag set in the config file you have to edit the config flie.

.. option:: quiet

    :type: bool
    :default: False

    If this option is set the output of current file names in the main loop is supressed.
    This option is useful when the script runs without supervision and output is not wanted.
    Logfiles are still written and if python encounters an error the crash is still written to the command line.

.. option:: no_expand

    :type: bool
    :default: False

    By default the script assumes a subfolder structure for the input files.
    For text files this is the folder named after the year, for xml files it is '_xml' appended to the year.
    When this flag is set this behaviour is supressed and all output files are placed in one directory.

.. option:: no_offline

    :type: bool
    :default: False

    This flag prevents the usage of any offline files.
    The input path is still required as the downloaded files are stored there.
    They are deleted after the week was parsed.

.. option:: update

    :type: bool
    :default: False

    This option puts the script into update mode.
    In this mode already downloaded files are skipped.
    Not already downloaded files will be downloaded and parsed.

    If you specify a year through the command line this year will be used.
    If not the script will run for the current year.

.. option:: applications

    :type: bool
    :default: False

    When enabled the scraper works on patent applications instead of granted patents.
    This option was tested only for newer versions (starting from 4.0) of the patent data system.

years
-----

In this section the start and end year are specified.

.. option:: start

    :type: int
    :default: 1976

    The first year the script should be run for.

.. option:: end

    :type: int
    :default: 2020

    The last year the script should be run for.
