######
Rules
######


Rules
======

Independent Claims
-------------------

Rule 1 (Independent level)
    A line with level 2 or higher cannot be independent.

Rule 2 (First claim independent)
    The first level 1 line of any patent is **always** independent.

Since the first first line in any claim is level 1, in practice this means that the first line in any patent is always independent.
Since my writer collapses all level 1 lines into one and uses the sequence of the first, it is enough to set the first line independent, to set the entire first claim independent.
This sometimes causes problems in applications, where I've seen patents that contain a claim reference in their first claim.

Rule 3 (Independent claim)
    A claim is independent, if its level 1 line does not contain any reference to an other claim.

To check for a reference to a preceding claim, I go through the list of :ref:`claim spellings <general-claim-spellings>`.
This rule has one unintended consequence.
Say a claim contains a reference, but is malformatted and the reference is in a level 2 line.
By this rule the claim might still be independent, even though it isn't.
I don't know if this is allowed drafting, but I've only spotted it in a few, otherwise very poorly formatted, applications.
So I don't believe this is a serious problem.


Dependent Claims
----------------

These rules only apply to lines with level 1!

Rule 4 (Explicit references)
    A claim that explicitly references another claim is assigned the sequence of the **first** level 1 line of the referenced claim.

In the xml versions, I first check the claim for any claim reference tags and then extract the claim id from the reference attribute.
This works very well for xml patent grants and slightly less so for applications.
For some reason applications are often worse drafted and the attributes do not correspond to the claim cited in the text.
This is corrected by the parsers, the rules simply accept the attribute.
When I do not find any xml references, I also look through the text for different forms of :ref:`explicit claim references <general-claim-spellings>`

Since in practice for each claim the first line is always level 1, this means that I always assign the dependency of the first line from the referenced claim.
An alternative would be to assign the last level 1 line of the referenced claim.

Rule 5 (Preceding claim)
    A claim that references a :ref:`preceding claim <preceding-claim-spellings>` is assigned the sequence of first level 1 paragraph of the preceding claim.

Rule 6 (Preceding claims)
    A claim that references multiple :ref:`preceding claims <preceding-claims-spellings>` is assigned the sequence of the previous claim.

Rule 6 is a strong deviation from the original rule set, where in the case of multiple preceding claims the claim was assigned the first claim.
If I find hits for both one and several preceding claims, I go with the result for several preceding claims.
If I find explicit references and references to either one or multiple preceding claims, I go with the explicit reference.
This might be relevant, since e.g. `any of the preceeding claims 1, 2, 4-6` would reference claim 1.

Rule 7 (Undefined reference)
    If a claim contains a reference, but I cannot determine which claim is referenced, I assign as dependency the sequence of the first claim.

Rule 8 (Invalid reference)
    If a claim contains an invalid reference, I assign as dependency -1.

This deviates from the given rules, where invalid references are given the sequence of the first claim.
In practice the most common reason for invalid references are references to itself, a later, or even non-existing claims.
Again, this can be found especially in patent applications.

Exception to Rule 8
    If a claim with references a later claim, but the claim id of the previous claim is a substring to the referenced claim, set as dependency the sequence of the previous claim.

This exception assumes that in the drafting process due to data errors e.g. a 1 might be added to the claim id.

Rule 9 (Cancelled claims)
    Claims marked as cancelled are marked by a dependency of -2.

This is not checked by the rules, but by the parser.
It is enabled only for applications.

Level 2 or higher
------------------

Rule 10 (Higher levels)
    A line of level 2 or higher is assigned as dependency the sequence of the last line with a lower level.

In practice, as each claim begins with a level 1 paragraph this means that level 2 or higher paragraphs cannot reference anything outside of the claim.
Therefore, I do not check them for explicit references.


Implementation of the Rules
============================

This section describes how the apply rules function works.
The code itself is documented, but since the function is very long and confusing it is not an easy read.
Here I want to give a bird perspective view of steps performed by the function.

1. Rule 2
    If the sequence is 1, meaning I process the first claim, I set the dependency to zero.
2. Rule 9
    I check if the level is larger or equal to 2.
    If it is, I search the claim for the last line with a lower level and select its sequence as dependency.
3. XML Reference
    Next I check for an XML reference tag.
    To do so, I iterate over the line's children and extract the attributes.
    When I find several elements, I take the smallest reference.
4. Text References
    When I have not found a reference in the previous step (e.g. because I have no XML element), I check if the text contains any of the :ref:`general claim spellings <general-claim-spellings>`.
    If it does, I keep the text to the right of the first occurence of this keyword.
    Note, that this step is also supposed to be triggered, whenever there is a reference to one or multiple preceeding claims.
5. Independent Claims
    If I've found nothing in step 3 and 4, the claim is independent.
6. Parsing Text References
    If I've found a reference in step 4, I need to extract the number from the text.
    To do so I look at the next 5 words.
    For each word, if this word has at most 5 characters, I remove all non-numeric characters.
    If the word contains any character indicating a common unit, I skip this word.
    I then take the smallest number found this way as the claim reference.
7. Interpreting Numeric Reference
    When I found a numeric reference, either in step 3 or from parsing a result in step 6, I need to check that this is a valid reference and extract the dependency.
    Specifically, I check that the reference is to claims before the focal claim, but larger than 0.
    I also check the exception to rule 8.
8. Preceding Claims
    In this step I check the keywords for preceding claims.
    If I find any of these keywords, I assign the dependency and return it.
    Note that I don't check all keywords, but stop on the first hit.
    Furthermore, I check first for one preceding claims and then for multiple, this might create problems if they yield different dependencies.
9. No Reference Parsed
    If I made it here, but failed to parse any reference, then I assign as dependency the first claim.



Claim Spellings
===============

.. _general-claim-spellings:

General Claim Spellings
----------------------------------

* ' claim '
* ' claims '
* ' claimed in '
* ' recited in '
* ' claimed '
* ' claim, '
* ' claims, '
* 'claim.'
* 'claims.'


.. _preceding-claim-spellings:

Preceding Claim Spellings
---------------------------------

* 'preceding claim'
* 'previous claim'
* 'aforementioned claim'
* 'claimed earlier'
* 'claimed above'
* 'claimed previously'


.. _preceding-claims-spellings:

Preceding Claims Spellings
-------------------------------

* 'preceding claims'
* 'previous claims'
* 'any one of claims'
* 'any preceding claim'