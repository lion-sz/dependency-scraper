===============
Introduction
===============

This script parsers the dependency structure from US patents, published in the US Patent and Trademark Office
through their `Bulk Data Storage System <https://bulkdata.uspto.gov>`_.
It runs on both the fixed width text files and the xml files and therefore processes the entire range from 1976 though today (2021).
Furthermore, it works for the patent format from googles public patent database and thus also for many older patents.


Output format
==============

For every input file the script writes a csv file.
The header is not included in the file, the columns are as following:

    1. 'PatentID'
    2. 'TE': does this line contain a table or equation?
    3. 'TE_global': does this patent contain a table or equation?
    4. 'CHEM': does this line contain a chemical formula?
    5. 'CHEM_global': does this patent contain a chemical formula?
    6. 'claim_id': parsed claim id.
    7. 'sequence': sequence number of the lines, starting at zero
    8. 'level': The level of the paragraph.
    9. 'dependency': The parsed dependency.
    10. 'text': The lines text.

Notably, some patents contain paragraphs, that are intercepted by other paragraphs.
An example of this is the patent US10527080, claim 1:

    "\1\. Fastener for the temporary assembly of at least two pre-drilled structures, comprising the following:

        a body extending along an axis, said body having a bearing surface substantially perpendicular to the axis, said surface being able to come into contact with a front surface of the structures to be assembled;

        a spacer, secured to said body;

        two elastic half-clips designed to pass through holes in the structures, each clip having a hooking spur that can be applied to a rear surface of the structures to be assembled, the spacer being located between the two half-clips;

        a mechanism that can be moved relative to the body, said mechanism comprising a threaded rod placed coaxially to the body, with one end of said rod connected to the half-clips, said mechanism also comprising a control nut having control nut threads mounted on said rod;

    said fastener being characterized in that the control nut comprises:

        an internal tapping cooperating with threads of the rod;

        an external thread cooperating with an internal tapping of the body;

    the rod threads and of the nut threads being opposite handed."

In this example the level 1 paragraph is continued in line 7 by line 6 and the final line.
However, as line 7 and 8 both depend on the middle part and simply joining all parts would omit this information.
To capture this nuance, the parser assigns the sequency and parses the dependency as the lines appear above.
Therefore, the lines 7 and 8 can depend on line 6.
In order not to clutter up the output file, the paragraph parts are joined in the output file, the line 6 and 9 do not appear in the output file.
The joined lines are identified by their sequence in double square brackets at the beginning of the line:

    "1. fastener for the temporary assembly of at least two pre-drilled structures, comprising the following: [[6]] said fastener being characterized in that the control nut comprises: [[9]] the rod threads and of the nut threads being opposite handed."

Later claims that reference this claim however, will always reference the first level 1 line in a claim.
For this example, this would be 1.

Implemented Formats
====================

Currently implemented and tested are:

* APS (1976 - 2001)
* XML Format 2.5 (2002 - 2004)
* XML Format 4.0-5 (2005 - now)
* Google Public Patents (all years, recommended pre 1976)
* My custom USPTO XML format 4.0 for canadian patents.

Historic Patents
----------------

The historic patents are provided by `Google Biq Query <https://console.cloud.google.com/marketplace/partners/patents-public-data>`_, I use the patent descriptions and extract the claims myself.
I have to do this, since for early patents the claims are often either completely missing or poorly extracted.
My code is rather aggressive and thus finds more claims than google does.
The quality of the extracted claims however is not perfect.
Therefore, for all years after 1976 the data provided by the patent office is superior.

Changelog
==========

v2.1
----

* PATFT parsing removed
* and replaced by historic parser
* Application parsing
* Claim ID correction
* Claim extraction project merged
* Writing of new ruleset, refactoring of rules function

v2.0
----

* Introduction of PATFT parsing
* Bugs in the ruleset
* Major refactor
* Unittests for the rules

Common Issues
=============

When parsing the patents there are a number of common problems.
Here I want to outline the most frequent and what my code does when encountering them.
These are all problems that occur almost exclusively in patent applications and historic patents.
The modern (APS and XML) granted patents contain almost no errors.


* Wrong claim attributes
    The XML applications often contain missing claim attributes, either in the claim id or in the referred claim id.
    Therefore I try to extract all claim id (either in the beginning of a claim or in a claim reference) from the text and prefer these to the XML attribute.
* Invalid references
    The v2.0 code did not allow for invalid references, my current code does so by assigning -1 to the dependency field.
    This happens when I find ad reference to a claim that is not in the data.
    Common causes are either plain errors in the references, missing claims, or claims that I failed to properly extract.
* References to cancelled or invalid claims
    Some claims reference either cancelled or invalid claims.
    My code then marks these claims as cancelled or invalid as well (so a dependency of -2 or -1 respectively).
    This for one causes all lvl2 or higher lines or a cancelled or invalid claim to be cancelled or invalid as well, but can also cause large sections of a patent to be marked this way.
    Depending on how the data should be used, this might be a problem.
    The relevant code is in the writer class.
* Malformatted patents
    Applications or historic patents are poorly formatted.
    I've implemented a few approaches to deal with missing or duplicated claims, those are described in the section :doc:`corrections`.