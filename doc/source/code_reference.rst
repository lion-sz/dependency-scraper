Code reference
==============

This reference is constructed from the doc strings already included in my python code.

It is build (almost exclusively) from the source files and meant as a guide to alter the source files.
Therefore, if you do alter the source code please update the relevant docstrings so that the documentations stays up to date.

The script has three main components:

* the module :class:`src.bulkdata_parser.Bulk` downloads and manages the bulkdata files,
* the :class:`src.writer.Writer` writes the output to disk and
* the parsers parse the xml files and therefore do the main work.

Furthermore, there is a module that contains the function :func:`src.rules.apply_rules`, used to classify the dependency of every paragraph.

Rules
"""""
.. automodule:: src.rules
    :members:


Parsers
"""""""

There are several different parser for the different versions of the input data.
These share a common interface to the rest of the code which is managed by the  :class:`src.interface_parser.Parser`.

Interface parser
****************

.. automodule:: src.interface_parser
    :exclude-members: version_calls, version_headers
    :members:


Parser API
**********

All parser share a common interface, documented here.
It documents only the public elements that all parser share and can thus be used as a reference when
altering the :class:`src.interface_parser.Parser`.

.. class:: ExampleParser

    .. py:method:: open(file)

        Opens and prepares the next file. It returns a new boolean indicating the functions success.
        It must be called before the iterator.

    .. py:method:: __iter__()

        This class provides an iterator over all patents in an opened file.

    .. py:method:: __next__()

        Returns reads in and returns the next patent's claim section and number.
        Both are None on failure.
        The claims section is either a nested list for the text years or
        a :class:`lxml.etree.Element` for the XML years.

    .. py:method:: parse_claims(claims, pat_nbr)

        Parse the claims that were returned by the iterator.
        The return is None, output is written to the writer.

There are two main parser, a text parser and an xml parser.
The text parser has children for the two formats, APS and PATFT.
The xml parser has one child class, the version 4 parser, which in turn has several further children for the other xml versions.

Bulkdata parser
"""""""""""""""
.. automodule:: src.bulkdata_parser
    :members:


Writer
""""""
.. automodule:: src.writer
    :members:

