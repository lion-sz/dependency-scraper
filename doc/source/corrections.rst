############
Corrections
############


Especially historic patents and applications often contain errors in the claim ids.
In my code I try to fix a few of these errors.

Fixing one error might give information that allows me to fix another error, especially improving the claim splitting or fixing duplicate claims.
Therefore, I run these three steps in the order below until no step made any change to the patent.


Unreasonably High Claim IDs
===========================

Some claims have very high claim ids (some patents even over 1000).
These are usually data errors, e.g. what I read in as a claim id is not a claim id.
Since this can cause further problems in the other correction steps, I try to fix this.
I classify claims ids as unreasonable, if their id is higher than `1.2 * number claims + 30`.
Unreasonable claims are then removed and the lines are appended to the previous claims.


Mislabeled Claim
================

Sometimes claims are mislabeled, e.g. claim 5 appears as claim 15.
I look for such mistakes by going through the patent and comparing groups of three claims, where first claim id and the last claim id are i and i + 2, but the middle claim id is not what I would expect (i+1).
If this is the case, I can assume that the claims are labelled through, except that the middle claim was mislabeled and I correct t it to i+1.
This approach does not work for the last claim, as there I cannot differentiate between mislabeled and missing claims.


Not Splitted Claims
===================

A further problem are claims that are not split properly.
This is especially the case in the historic claims, since my claim splitter program is missing some logic.
I've implemented this here, instead of the claim splitter, so that I can run this for applications as well.

The function works by looking for gaps in the claim ids, say the claims go from 2 directly to 4.
In this case, claim 3 is missing, so I look through the claims text of claim 2 and see, if I can find the string '3. '.
If I do, I split the claim at this string and insert the right part as a new claim.
If the claim 2 has multiple claim lines, I go through them in order and split at the first occurrence of the wanted claim id.
This allows me to extract multiple claims, e.g. if claim 2 contains not only claim 3, but also claim 4.
I also apply the same procedure to the last claim.

When I split claims, I manage only the text and remove the XML element for both the new claim and the claim I've splitted.
This should not be a big problem, since especially for the historic patents there aren't claim reference tags anyway.
If this is a common problem in applications as well, it might be a further improvement, to keep the XML tags for applications.


Duplicate Claims
================

I want to make sure that there are no duplicate claim ids.
I try my best to prevent this by trying to correct common errors that cause duplicates (like an '11' becoming an '1I' and being read as a 1), but this is not always possible.
Therefore, I've written a function that tries to make sense of duplicate claims and - if that does not work - simply moves them to the end.
My approach does not change any claim ids I've parsed from the document, except those of duplicate claims.

I work on groups of at least two claims with the same claim id, one group at a time. In each call to the method I will work on only one group. For each group I check two steps:

1.  I check if for the claims the expected claim id (so the id of the previous claim plus one) is missing.
    If I find a (or several) claim for which the expected id is missing, but I have at least one of of the claims for which the given id makes sense, I assign the expected if to those claims.
    This step effectively fills in gaps in the claim ids.
    It is therefore especially good at recognizing claims where an error in the text recognition caused nonsensical claim ids.
    It works similarly to the approach for mislabeled claims, except that it can also work if there are claims missing after the mislabeled claim.
2.  If I cannot find a probable claim id in the original position, I simply move any claim that seems out of place to the end of the patent and give it a new claim id.
    I decide which claim seems out of place again by the expected claim id.
    If the given claim id is not the expected one (based on the previous claim), I move the claim to the end.
    One small exception is the first claim, I move it if it's claim id is not 1.

There is one edge case to approach one concerning the last claim.
I define missing claims as all claim ids between one and the highest claim id in the patent that do not exist.
But by this definition, a duplicate of the highest claim id cannot be corrected, as it will never be missing.
To prevent this, I automatically set the highest claim id + 1 as missing.