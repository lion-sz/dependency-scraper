.. claim scraper documentation master file, created by
   sphinx-quickstart on Sun Mar  1 22:44:50 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

USPTO Claim Scraper
===================

This project extracts the dependency structure of US patent claims following 1971.



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Introduction <introduction.rst>

   Rules <rules.rst>

   Corrections <corrections.rst>

   Usage <usage.rst>

   Claim Extraction <extraction.rst>

   Fixed width parser <fwparser.rst>

   Detailed code documentation <code_reference.rst>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

