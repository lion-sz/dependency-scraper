##################
Claim Extraction
##################

Since on Google Big Query claims are available only for a small share of patents, I use the descriptions for these patents.
These are available for more patents, but I need to extract the claims first.
To do so, I've written a small module.

I use three strategies for finding the claims:

1. Claim Statement
    The first strategy is to look for a claim statement and then use the claim ids to parse the structure.
    I also check any text that comes after the claim statement in the same paragraph for a first claim.
    This approach works best for well structured patents and is quite frankly the most reliable.
    But unfortunately, not all patents have this standardized claim statement or are structured well enough for this to work.
    So I have two more methods for extracting claims.
2. Claim IDs
    The second approach is meant for patents that have the claim id structure, but not the claim statement.
    I go through the patent from the end and check the 8 preceding paragraphs for a claim id.
    If i find one, I save the claim and check the 5 paragraphs before this claim for an other claim id.
    I do this until I cannot find a claim id in the preceding 5 paragraphs, in which case I stop.
3. One Claim
    The final approach is meant for patents that have a claim statement, but lack proper claim id structure.
    I look for the claim statement and assume that anything following it is a single claim.
    This allows me to extract some useful information even from very poorly structured patents.


Claim ID Corrections
=====================

Sometimes the text recognition mistakes a 1 for an 'I' or 'l'.
This messes up my claim id parser, I either do not find a claim id at all, or I find the wrong claim id, e.g. an '11.' might become a '1I.' and then be read in as a 1.
This is a also a common cause of duplicate claims.
To fix this, I check the first 5 characters of any paragraph that I check for a claim id and correct 'I' and 'l' to 1.
These corrections are not visible in the final text, but are applied only during the extraction.
