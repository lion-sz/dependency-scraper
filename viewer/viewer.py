from flask import Flask, request, redirect, render_template, Response
import lxml.etree as etree
import pandas as pd
import random
import logging
import sys
import pathlib
import json
from typing import Optional
sys.path.append('/home/lion/projects/uspto/')


from src.parse_xml import *
from src.bulkdata_parser import Bulk
from src.extraction import Extraction


defaults = {'input': '/nextcloud/pats/applications/',
            'out': '/data/tmp/out/', 'tmp': '/data/tmp/'}
start = '''<?xml-stylesheet href='static/claim.xsl' type='text/xsl'?><patent pat_nbr="{}" date="{}">'''
test_data_path = pathlib.Path('/home/lion/projects/uspto/test_data/patents/')


def to_text(x):
    return etree.tostring(x, encoding='utf-8', pretty_print=True).decode()
def to_dict(x):
    return dict(cid=x['cid'], seq=x['seq'], lvl=x['lvl'], dep=x['dep'], text=x['text'])
def to_line(x):
    line = etree.Element('line')
    line.set('cid', x['id'])
    line.set('seq', x['seq'])
    line.set('lvl', x['lvl'])
    line.set('dep', x['dep'])
    line.text = x['text']
    return line


class Parser:
    """
    When I want to select a patent directly, I can simply set the pat_nbr key.
    """

    def __init__(self, input, output, application, version, week):
        logging.getLogger('PatViewXML')
        self.week = week
        # Set up the parser
        if version == 'historic':
            self.parser = ParseHistoric(None)
            self.extractor = Extraction()
        if version == "canadian":
            self.parser = ParseCanadian(None)
        elif version == '1':
            self.parser = ParseV1(None, True)
        elif version == '2':
            self.parser = ParseV2(None, False)
        else:
            self.parser = ParseV4(None, application)

        self.parser.open(input)
        self.pats = dict([(pat_nbr, to_text(claims)) for claims, pat_nbr in self.parser if pat_nbr is not None])
        self.keys = list(self.pats.keys())
        random.shuffle(self.keys)

        names = ['pat_nbr', 'te', 'teg', 'chem', 'chemg', 'cid', 'seq', 'lvl', 'dep', 'text']
        self.results = pd.read_csv(output, names=names,
                                       dtype=dict([(x, str) for x in names]))
        self.results.filter(['pat_nbr', 'cid', 'seq', 'lvl', 'dep', 'text'])
        self.results_unique = pd.unique(self.results['pat_nbr'])

        self.pat_nbr = None

    def next(self):
        """
        Select the next patent, no return.
        """
        while True:
            if len(self.keys) == 0:
                self.pat_nbr = None
                print("No more patents.")
                return
            self.pat_nbr = self.keys.pop()
            if self.pat_nbr[0] not in ['D', 'H', 'T', 'P']:
                break
        print(self.pat_nbr)

    def fetch_results(self):
        if self.pat_nbr is None:
            return None
        if self.pat_nbr not in self.results_unique:
            print('patent not found in data')
            return None
        parsed = self.results.loc[self.results['pat_nbr'] == self.pat_nbr]
        if len(parsed) == 0:
            print('no data')
            return None
        parsed = [to_dict(row) for ind, row in parsed.iterrows()]
        print(len(parsed))
        return parsed


class TextParser:

    def __init__(self, file):
        self.dat = pd.read_csv(file, header=None)
        self.dat.columns = ['pat_nbr', 'te', 'teg', 'chem', 'chemg', 'cid', 'seq', 'lvl', 'dep', 'text']
        self.dat.filter(['pat_nbr', 'cid', 'seq', 'lvl', 'dep', 'text'])

        self.pats = list(pd.unique(self.dat['pat_nbr']))
        random.shuffle(self.pats)

    def next(self):
        self.pat_nbr = self.pats.pop()

    def fetch_results(self):
        parsed = self.dat.loc[self.dat['pat_nbr'] == self.pat_nbr]
        if len(parsed) == 0:
            return None
        parsed = [to_dict(row) for ind, row in parsed.iterrows()]
        return parsed


app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True


@app.route('/', methods=['GET', 'POST'])
def index():
    global bulk
    global form
    global parser
    global application
    # Return the template for get methods
    if request.method == 'GET':
        return render_template('index.html', defaults=defaults)

    # Check the completeness of the form
    form = request.form
    print(form)
    application = form['app'] == 'True'

    if (form['input'] == '' and form['version'] != 'text') or\
            form['out'] == '' or (form['year'] == '' and form['version'] not in ['historic', "canadian"]):
        return 'Incomplete Form'

    # For the historic parser I can directly initialize the parser.
    if form['version'] == 'historic':
        parser = Parser(form['input'], form['out'], False, 'historic', '')
        return redirect('/patent')

    if form["version"] == "canadian":
        print("using canadian")
        parser = Parser(form["input"], form["out"], False, "canadian", "")
        return redirect("/patent")

    # for the text parser I don't need the bulkdata parser.
    if form['version'] != 'text':
        # Set up the Bulkdata parser
        bulk = Bulk(data_path=form['input'], tmp_path=form['tmp'] if form['tmp'] != '' else None,
                    applications=application)
        bulk.open_year(int(form['year']))
    return redirect('/select_week')


@app.route('/select_week', methods=['GET', 'POST'])
def select_week():
    global parser
    global text_path

    if request.method == 'GET':
        # first get the available files:
        # Text version:
        if form['version'] == 'text':
            text_path = pathlib.Path(form['out']) / form['year']
            file_list = [f.name for f in text_path.glob('*.csv')]
            file_list.sort()
            if len(file_list) == 0:
                return 'No files found.'
            print(file_list)
        elif form['version'] == 'historic':
            # Not yet implemented. But I probably don't need any selection here.
            return 'Not yet implemented.'
        else:
            file_list = bulk.zip_list

        return render_template('week.html', files=file_list)
    week = request.form['week']
    if week == '':
        return 'No week selected'

    # Text version:
    if form['version'] == 'text':
        parser = TextParser(text_path / week)
    elif form['version'] == 'historic':
        # Not yet implemented
        return 'Not yet implemented.'
    else:
        # Get the file paths
        input_file, target_file = bulk.prepare_xml(week)
        output_file = pathlib.Path(form['out']) / form['year'] / target_file.name.replace('xml', 'csv')
        target_file = pathlib.Path(form['tmp']) / target_file
        print(output_file)
        print(target_file)
        parser = Parser(target_file, output_file, application, form['version'], week)

    return redirect('/patent')


@app.route('/patent')
def patent():
    parser.next()
    parsed = parser.fetch_results()
    if parsed is None:
        return 'No more data.' if parser.pat_nbr is None else 'Error.'
    if form['version'] == 'text':
        return render_template('patent_text.html', parsed=parsed, pat_nbr=parser.pat_nbr)
    else:
        return render_template('patent.html', parsed=parsed, pat_nbr=parser.pat_nbr)


@app.route('/<pat>')
def get(pat):
    if pat not in parser.pats:
        return 'Patent not found'
    parser.pat_nbr = pat
    parsed = parser.fetch_results()
    if form['version'] == 'text':
        return render_template('patent_text.html', parsed=parsed, pat_nbr=parser.pat_nbr)
    else:
        return render_template('patent.html', parsed=parsed, pat_nbr=parser.pat_nbr)


@app.route('/source')
def source():
    if form['version'] == 'historic':
        pat = parser.extractor.parse(parser.pat_nbr, etree.fromstring(parser.pats[parser.pat_nbr]))
        if pat is None:
            return 'For some reason the claim extraction did not work.'
        pat = to_text(pat)
    else:
        pat = parser.pats[parser.pat_nbr]
    return Response('<?xml-stylesheet type="text/css" href="static/claims.css"?>\n' +\
                    pat, mimetype='application/xml')


@app.route('/write')
def write():
    """
    Simply dump the current patent as a lxml tree.
    The tree contains two elements, claims which stores the claims tree and results, which
    stores the results lines.

    The output is written to test_data/patents/pat_nbr.xml.
    """
    # Writing the text version
    if form['version'] == 'text':
        out_file = test_data_path / f'{parser.pat_nbr}.json'
        with out_file.open('w') as jfile:
            json.dump(parser.fetch_results(), jfile, indent=4)
        return redirect('/patent')

    # Writing the xml versions
    tree = etree.Element('patent')
    tree.set('pat_nbr', parser.pat_nbr)

    claims = etree.Element('claims')
    claims_str = parser.pats[parser.pat_nbr]
    claims.append(etree.fromstring(claims_str))
    tree.append(claims)

    result = etree.Element('results')
    table = parser.fetch_results()
    if table is None:
        print('table is None')
        return 'KO'
    for line in table:
        result.append(to_line(line))
    tree.append(result)

    if application:
        if form['version'] == '4':
            ending = '_app.xml'
        elif form['version'] == '1':
            ending = '_app_v1.xml'
        else:
            return f'Unrecognized application version {form["version"]}'
    else:
        if form['version'] == '2':
            ending = '_v2.xml'
        elif form['version'] == '4' or form['version'] == 'historic':
            # Version 4 and the historic patents can be differentiated by the patent number.
            ending = '.xml'
        else:
            return f'Error: Version {form["version"]} cannot be written.'
    filename = test_data_path / (parser.pat_nbr + ending)
    with filename.open('w') as ofile:
        ofile.write(to_text(tree))

    return redirect('/patent')


form = None
application = False
parser: Optional[Parser] = None
bulk: Optional[Bulk] = None
text_path: Optional[str] = None

