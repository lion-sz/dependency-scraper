#!/usr/bin/sh

#
# This little script is supposed to build a release zip file
# that can be send to people.
#
# This zip file must include all python modules, the config file and
# both an html and an pdf version of the documentation
#

# get the most recent version
version=$(git tag | tail -n 1)

# if there is a cache, delete it.
if [[--d src/__pycache__]]
then
    rm -r src/__pycache__
fi

# copy the source 
mkdir uspto
cp uspto.py uspto
cp uspto.ini uspto
cp -r src uspto
cp run.sh uspto

# creat the documentation
# first the html
pushd doc
make clean html
cp -r build/html ../uspto/html_doc
# then the pdf
make clean latexpdf
cp build/latex/claimscraper.pdf ../uspto/doc.pdf

# and finally zip
popd
zip -r "uspto_""$version"".zip" uspto
rm -r uspto
