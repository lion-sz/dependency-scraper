# Todo:

## Correctly parsing dash.
Some patents specify 'any preceding claims 1-3'.
Therefore, I need to further amend the rule function, especially the parse words function.



## Parsing the patent structure

A good example of the problem is 10527080 in the first 2020 week.
This has first lvl1 text, than lvl2 pars and then falls back to lvl1 text.
This is parsed correctly, the brackets specify of which sequences the lvl1 par comprises of.
But I need to document this feature and how to read this.
**Add a note to the documentation, that there might be holes in the sequence.**
Another good example is 10526132.


## high priority:


## medium priority:
- add a command flag such that a single file can be specified.
- better parsing of text containing equations! especially the part after equal signs
- check if the script sucks on a hdd or if that is somewhat okay.
- benchmark cpu and more importantly memory performance
