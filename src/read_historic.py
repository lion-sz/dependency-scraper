import pathlib
import json
from typing import Generator, Tuple, Optional


def read(path: pathlib.Path) -> Generator[Tuple[str, Optional[str]], None, None]:
    """
    This function reads in the patents from the json file.
    """
    with open(path, 'r') as ifile:
        for line in ifile:
            try:
                tmp = json.loads(line)
                yield tmp['publication_number'], tmp['description_localized_html'][0]['text']
            except IndexError as err:
                # There was an empty patent descriptions.
                yield tmp['publication_number'], None

