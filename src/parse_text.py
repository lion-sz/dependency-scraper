import logging
import sys
import pathlib
from typing import Union, Tuple, List, Optional
import re

from src.rules import apply_rules
import PyFWParser as fwparser


class ParseText:
    """
    This class is a superclass to the PATFT and APS parsers.
    """

    doc_bound = None
    relevant_keys = ['WKU']
    section = []

    keys = {}
    par_keys = []
    end_keys = []
    te_keys = []

    skip_lines = 1
    patft_reg = re.compile('US_PATFT_BRS_Full_Text_Extract_197[1-6]_1.txt')

    claim_id_reg = re.compile(r'^([0-9]+)\.')

    def __init__(self, writer, application):
        self.writer = writer
        self.logger = None
        self.entry = None

        fwparser.doc_bound(self.doc_bound)
        fwparser.set_keys(self.relevant_keys)
        fwparser.set_sections(self.section)

        if application:
            print('Applications not jet allowed.')
            sys.exit(-1)

    def __del__(self):
        # I need to reset the parser in case there is another call right after
        fwparser.reset()

    def open(self, infile: pathlib.Path) -> bool:
        """
        Open the file. Assumes that the fwparser module is already prepared.
        Since the first PATFT file in each year does not contain claims information,
        I check if I'm opening this file.
        If so, I log an info and return False.

        :param infile: The file path.
        :type infile: :class:`pathlib.Path`
        :return: If the opening succeeded.
        :rtype: bool
        """
        fwparser.close_file()

        if self.patft_reg.match(infile.name):
            self.logger.info('Skipping first PATFT file {}.'.format(infile.name))
            print('skipping file')
            return False
        # Maybe add a statement to catch an error here.
        try:
            fwparser.open_file(str(infile), self.skip_lines)
        except RuntimeError as err:
            self.logger.critical('Opening text file failed: {}'.format(str(err)))
            return False
        return True

    def __iter__(self):
        """
        There is no work to do when setting up the iterator.
        I simply look for the next patent in the next method.
        :return: self
        """
        return self

    def __next__(self) -> Union[Tuple[None, None], Tuple[List[Tuple[int, List[Tuple[int, str]]]], str]]:
        self.entry: List[Tuple[str, str]] = fwparser.get()
        if self.entry is None:
            raise StopIteration
        pat_nbr = self._get_pat_nbr(self.entry)
        if pat_nbr is None:
            return None, None
        claims: List[Tuple[int, List[Tuple[int, str]]]] = self._build_claim_struct(self.entry, pat_nbr)
        if claims is None:
            return None, None
        return claims, pat_nbr

    def _get_pat_nbr(self, entry: List[Tuple[str, str]]) -> Optional[str]:
        for key, val in entry:
            if key == self.keys['pat_nbr']:
                # Do I need to remove the trailing checksum character
                return self._clean_pat_nbr(val)
        return None

    def _clean_pat_nbr(self, pat_nbr: str) -> str:
        """
        Empty, only implemented by the subclasses.
        """
        return pat_nbr

    def _build_claim_struct(self, entry: List[Tuple[str, str]], pat_nbr: str) ->\
            Optional[List[Tuple[int, bool, List[Tuple[int, str]]]]]:
        """
        Parse the claim structure into a new nested list.
        Each claim has an number, a te flag and a sublist of tuples, containing the level and the text.

        The first line of any claim is always level 1.
        The next are level 2. Whenever there is a new tag I increase the level by one.

        I ensure that the id is an integer starting from zero, without missing values.

        :param entry: The entry returned from the parser.
        :type entry: List[Tuple[str, str]]
        :param pat_nbr: The patent number, used for logging errors.
        :type pat_nbr: str
        :return: A list of claim-nr, claims, each claim is a list of level and text.
        :rtype: List[Tuple[List[Tuple[int, str]]]] or None
        """
        claims = []  # Stores the parsed result
        tmp = []  # Stores one claim, appended to the list.
        par_key_stack = []
        claim_nr = 0
        te = False
        claim_section_started = False
        first_par = True
        for key, val in entry:
            if claim_section_started:
                if key == self.keys['claim_statement']:
                    # Some patents fail to properly set up the first claim and go from the
                    # claim statement directly to the first lvl2 paragraph, with the lvl1 in the
                    # claim statement.
                    # Here I check for this and if found change the key to lvl 1 and append to parsed claims.
                    if val is not None and '1.' in val:
                        self.logger.warning('Possible claims in the claim statement.', pat_nbr=pat_nbr)
                        val = val.split('1.', 1)[1]
                        key = self.par_keys[0]
                        claim_nr += 1
                if key == self.keys['claim']:
                    # New claim; append and reset, then compare the numbering schemes.
                    if claim_nr > 0:
                        claims.append((claim_nr, te, tmp))
                    claim_nr = claim_nr + 1
                    te = False
                    tmp = []
                    par_key_stack = []
                    first_par = True
                    # I could remove this part if it does not throw many errors.
                    try:
                        tmp_nr = int(val.replace('.', '').strip())
                        if tmp_nr != claim_nr:
                            self.logger.warning('claim number differ: {} parsed, but counted {}'.
                                                format(tmp_nr, claim_nr), pat_nbr=pat_nbr)
                    except ValueError as err:
                        self.logger.warning('invalid claim nr: {}'.format(str(err)), pat_nbr=pat_nbr)
                elif key in self.par_keys:
                    # In case of no value, simply skip.
                    if val is None:
                        continue
                    if claim_nr == 0:
                        self.logger.warning('No claim key before first text', pat_nbr=pat_nbr)
                        claim_nr += 1
                    # I understand this key, determine the level and append.
                    if first_par:
                        tmp.append((1, val))
                        first_par = False
                    else:
                        if len(par_key_stack) == 0:
                            tmp.append((2, val))
                            par_key_stack.append(key)
                        else:
                            if key == par_key_stack[-1]:
                                pass
                            elif len(par_key_stack) >= 2 and key == par_key_stack[-2]:
                                par_key_stack.pop()
                            else:
                                par_key_stack.append(key)
                            tmp.append((len(par_key_stack) + 1, val))
                elif key in self.te_keys:
                    te = True
                elif key in self.end_keys:
                    break
                else:
                    if not key == self.keys['claim_statement']:
                        self.logger.warning('Unexpected key in claims: {}'.format(key), pat_nbr=pat_nbr)
            elif key == self.keys['claims']:
                claim_section_started = True
        # append the last claim
        if tmp:
            claims.append((claim_nr, te, tmp))
        if not claims:
            self.logger.debug('No claims found', pat_nbr=pat_nbr)
            return None
        return claims

    def parse_claims(self, claims: List[Tuple[int, bool, List[Tuple[int, str]]]], pat_nbr: str) -> None:
        """
        Parse the claims and add them to the writer.
        :param claims: The claims, returned from the build claim method.
        :type claims: List[Tuple[int, bool, List[Tuple[str, str]]]]
        :param pat_nbr: The patent number
        :type pat_nbr: str
        :return: None
        """

        parsed_claims = {}
        sequence = 1

        for id, te, claim in claims:
            parsed_claims[str(id)] = []
            for lvl, text in claim:
                try:
                    dep = apply_rules(self.logger, {}, None, text, lvl, sequence, id, pat_nbr, parsed_claims)
                except KeyError as key:
                    self.logger.warning('Key {} not found in patent, setting -1'.format(str(key)), pat_nbr=pat_nbr)
                    dep = -1
                    parsed_claims[str(id)].append((lvl, sequence))
                self.writer.add_line(sequence, id, lvl, dep, text, te, False)
                sequence += 1
                # Right not I do not use the line id.
            self.writer.buffer_claim()
        self.writer.write_patent(pat_nbr)


class ParseAPS(ParseText):
    # I vaguely remember that there were PA7 tags instead of PA1. To see how many there are I record them here.
    doc_bound = "PATN"
    section = ['CLMS']

    keys = {
        'claims': 'CLMS', 'claim_statement': 'STM', 'claim': 'NUM', 'pat_nbr': 'WKU'
    }
    par_keys = ['PAR', 'PAL', 'PA0', 'PA1', 'PA2', 'PA3', 'PA4', 'PA5', 'PAC']
    te_keys = ['TBL', 'TBL3', 'EQU']

    skip_lines = 2

    def _clean_pat_nbr(self, pat_nbr: str) -> str:
        """
        The APS format does uses the trailing bit as check digit.
        I do not check the number, but simply remove it.
        """
        return pat_nbr[:-1]

    def __init__(self, writer, application):
        super().__init__(writer, application)
        fwparser.doc_bound('PATN')
        fwparser.set_keys(self.relevant_keys)
        self.logger = logging.getLogger('ParseAPS')


class ParsePATFT(ParseText):

    doc_bound = "*** BRS DOCUMENT BOUNDARY ***"
    section = ['CLMS']

    keys = {
        'claims': 'CLMS', 'claim_statement': 'CLST', 'claim': 'CLNO', 'pat_nbr': 'WKU'
    }
    par_keys = ['CLPR']
    end_keys = ['DSRC', 'SIZE', 'AISD', 'ISY']
    te_keys = ['CLTL', 'CLEQ']

    def __init__(self, writer, application):
        super().__init__(writer, application)
        fwparser.doc_bound('*** BRS DOCUMENT BOUNDARY ***')
        fwparser.set_keys(self.relevant_keys)
        fwparser.set_sections(['CLMS'])
        self.logger = logging.getLogger('ParsePATFT')