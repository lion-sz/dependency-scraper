"""
This module is used to parse newer xml versions, starting with v4.0.
Since these versions are largely identical, except for some minor changes in the xml tags,
I've defined one parser class :class:`Parse_v4x` that contains the code and all parser
implementing a specific 4.x version only provide three dictionaries with tags.
"""

import copy
import io
import logging
import re
import sys
from dataclasses import dataclass
from collections import defaultdict
from typing import List, Dict, Tuple, Optional
from zipfile import ZipFile

import lxml.etree as etree

import src.read_historic
import src.extraction
from src.rules import apply_rules
from src.rules import check_TEC


def to_text(x):
    return etree.tostring(x, method='text', encoding='utf-8').decode().strip().lower()


@dataclass
class Line:
    """Represents a line in a claim."""
    elem: etree.Element
    sequence: int
    level: int
    text: str
    te: bool
    chem: bool


@dataclass
class Claim:
    """Represents a claim"""
    id: int
    lines: List[Line]
    cancelled: bool

    def __init__(self, id: int, lines: List[Line], cancelled: bool = False):
        self.id = id
        self.lines = lines
        self.cancelled = cancelled


class ParseXML:
    """
    This class is supposed to do the actual work for the xml versions 4.0 - 4.5.

    These are identical except for maybe a few slightly different tags.
    Therefore I want just one class taking care of the logic and the three parser
    classes will take care of handling tags and making everything look clean.

    :param application: Should I work on applications instead of grants?
        If set all occurrences of grant are replaced bz application.
    :type application: bool
    """

    parse_keywords = {}
    patnbr_tags = []
    replace_declaration = ''

    def __init__(self, writer):
        self.writer = writer

    def open(self, file):
        """
        This method reads in a the file, removes the xml declarations
        and appends an enclosing root tag.
        The result is stored in a memory file for the iterparser.

        :param file: The filepath to be read.
        :return: was the opening successful.
        :rtype: bool
        """
        with open(file, 'r', errors='ignore') as ifile:
            xml = ifile.read()
        xml = self.reg.sub('', xml)
        self.memfile = io.BytesIO(('<root>\n' + xml + '\n</root>').encode('UTF-8'))
        return True

    def __iter__(self):
        self.context = etree.iterparse(self.memfile, events=('end',),
                                       tag=self.parse_keywords['pat_tag'], recover=True)
        self.iter_cont = self.context.__iter__()
        return self

    def __next__(self):
        """
        :return: A tuple of the claim section and the patent number.
        :rtype: Tuple[:class:`lxml.etree.Element`, str] or Tuple[None, None]
        """
        # A stopIteration raised by the context should just go up...
        event, elem = next(self.iter_cont)
        # Remove all previous tags
        while elem.getprevious() is not None:
            elem.getprevious().clear()
            del elem.getparent()[0]

        pat_nbr = self._get_patnbr(elem)
        if pat_nbr is None:
            return None, None

        claims = elem.find(self.parse_keywords['tag_claim_list'])
        if claims is None:
            self.logger.error('No claims found', pat_nbr=pat_nbr)
            return None, None

        return (claims, pat_nbr)

    def _get_patnbr(self, root) -> str:
        """Overwritten by the inheriting classes."""

    def _start_text(self, orig: etree.Element) -> str:
        """This method extracts the text from a possible nested claim text paragraph."""
        root = copy.deepcopy(orig)
        for elem in root.xpath(self.parse_keywords['xp_claim_par']):
            root.remove(elem)
        text = etree.tostring(root, method='text', encoding='utf-8')
        return text.decode().strip().lower()

    def parse_claims(self, claims, pat_nbr):
        """Implemented by subclasses"""


class ParseV4(ParseXML):

    parse_keywords = {
        'pat_tag': 'us-patent-grant', 'tag_claim_list': 'claims',
        'tag_single_claim': 'claim', 'attr_claim_id': 'id',
        'keywords_chem': ['chemistry', 'us-chemistry'],
        'tag_claim_text': 'claim-text', 'xp_claim_par': './claim-text',
        'reference_tag': 'claim-ref', 'attr_ref_id': 'idref',
        'keywords_te': ["us-math", "tables", 'maths']
    }

    patnbr_tags = [
        'us-bibliographic-data-grant', 'publication-reference',
        'document-id', 'doc-number'
    ]
    reg = re.compile(r'<\?xml version="1\.0" encoding="UTF-8"\?>|<!DOCTYPE .* \[')
    reg_claim_ref = re.compile(r'claim ([0-9]+)\.?')

    def __init__(self, writer, application, no_change_keywords=False):
        """

        :param writer: The writer class to which to write the patents.
        :param application: Whether the parser works on applications.
            If set, it looks for cancelled claims and also changes patent and the first patent number keywords.
        :param no_change_keywords: Changing the keywords is not required for the V1 application parser.
            Therefore, this flag allows that to be changed, while the application flag still enables cancelled claims.
        """
        super().__init__(writer)
        self.logger = logging.getLogger('ParseV4')
        self.application = application
        self.re_cancelled = re.compile(r'[0-9\. -:]*\(?cancel+ed\)?\.?')
        self.re_cancelled_one = re.compile(r'([0-9]*)\.?:? *\(?cancel+ed\)?\.?')
        self.re_cancelled_two = re.compile(r'([0-9]*)\.? *- *([0-9]*)\.?:? *\(?cancel+ed\)?\.?')
        if application and not no_change_keywords:
            self.parse_keywords['pat_tag'] = 'us-patent-application'
            self.patnbr_tags[0] = 'us-bibliographic-data-application'

    def claim_id(self, claim) -> int:
        return int(claim.get(self.parse_keywords['attr_claim_id']).split('-')[1])

    def _get_patnbr(self, root) -> Optional[str]:
        """
        Can be simplified to a few lines.
        :param root: The patent root as xml document
        :return: :class:`lxml.etree.Element`
        :return: The patent number on success, None on failure.
        :rtype: str or None
        """
        r1 = root.find(self.patnbr_tags[0])
        if r1 is None:
            self.logger.critical("r1 is None in get_pat_nbr")
            print("r1 is None in get_pat_nbr")
            sys.exit()
            r1 = root
        try:
            r2 = r1.find(self.patnbr_tags[1])
        except AttributeError:
            self.logger.critical('Second finding patnbr, at second tag')
            return None
        try:
            pat_nbr = r2.find(self.patnbr_tags[2])\
                    .find(self.patnbr_tags[3]).text
        except AttributeError:
            self.logger.critical('failed finding patnbr tag at tags 2-3')
            return None
        return pat_nbr

    def cancelled(self, claim: etree.Element) -> Optional[Tuple[int, int]]:
        """
        Some patent applications contain cancelled claims.
        These can cause confusion, as often multiple cancelled claims are specified in one tag
        (e.g. 2-3 (cancelled)).

        To parse this I use two approaches, first checking if the claim ID specifies a range and second,
        if the text matches '[0-9\. -]*(cancelled).?'.

        When not parsing applications, cancelled claims should not occur and therefore the result is always False.

        :param claim: The claim element
        :type claim: `lxml.etree.Element'
        :return: None, if there are no cancelled claims, (first, last) if there are cancelled claims.
        """
        if not self.application:
            return None
        if self.re_cancelled.match(to_text(claim)) is not None:
            text = to_text(claim)
            one = self.re_cancelled_one.match(text)
            if one is not None:
                return int(one[1]), int(one[1])
            two = self.re_cancelled_two.match(text)
            if two is not None:
                return int(two[1]), int(two[2])
            self.logger.error('failed {}'.format(text))
        return None

    def _rec_parse_struct(self, claim, claim_list, parent_level=0):
        """
        Recursively parse the claim structure into a nested list.

        :param claim: The current root xml tag.
        :type claim: :class:`lxml.etree.Element`
        :param claim_list: The list in which the results should be written.
        :type claim_list: list
        :param parent_level: The level of the parent claim.
        :type parent_level: int
        :return: None
        """
        text = self._start_text(claim)

        level = parent_level
        (TE, CHEM) = check_TEC(claim, self.parse_keywords)
        if text != '' or TE or CHEM:
            level = parent_level + 1
            self.sequence = self.sequence + 1
            claim_list.append(Line(claim, self.sequence, level, text, TE, CHEM))
        for child in claim:
            if child.tag == self.parse_keywords['tag_claim_text']:
                self._rec_parse_struct(child, claim_list, level)

    def _parse_claim_structure(self, claims: etree.Element) -> List[Claim]:
        """
        Parse the claim section into a list of claims.
        This is handled by a separate function, since I might need to
        check for poor line numbering.

        Each Tuple describes a claim and contains a parsed claim, the claim list and
        an indicator for cancelled cancelled claims.
        """
        claim_list: List[Claim] = []
        self.sequence = 0
        for claim in claims.iter(self.parse_keywords['tag_single_claim']):

            # Check for cancelled claims and write them to the claim list.
            cancelled = self.cancelled(claim)
            if cancelled is not None:
                for i in range(cancelled[0], cancelled[1] + 1):
                    self.sequence += 1
                    claim_list.append(Claim(i, [Line(None, self.sequence, 1, to_text(claim), False, False)], True))
                continue

            # Parse a normal claim.
            tmp_list: List[Line] = []
            for child in claim:
                if child.tag == self.parse_keywords['tag_claim_text']:
                    self._rec_parse_struct(child, tmp_list, 0)
            claim_list.append(Claim(self.claim_id(claim), tmp_list, False))

        return claim_list

    def _correct_claim_references(self, pat_nbr: str, claims: etree.Element):
        """
        Some applications contain errors in the claim reference attributes.
        Therefore, I want to look through all claim-reference tags and
        (if they are different) replace the attribute with the value in the text.
        """
        for ref in claims.findall(f'.//{self.parse_keywords["reference_tag"]}'):
            try:
                if self.reg_claim_ref.match(ref.text) is not None:
                    text = self.reg_claim_ref.match(ref.text).groups()[0]
                    attr = str(int(ref.get(self.parse_keywords['attr_ref_id']).split('-')[1]))
                    if text != attr:
                        ref.set(self.parse_keywords['attr_ref_id'], f'CLM-{text}')
            except Exception as err:
                self.logger.exception(err, pat_nbr=pat_nbr)

    def _fix_high_claim_ids(self, pat_nbr: str, claims: List[Claim]) -> None:
        """
        Sometimes I parse very high claim numbers (e.g. above 500).
        In this function I look for these cases and append them to the previous claim.

        My rule for finding unreasonably high claims is that the claim number is higher than the bigger of
        5 or 2 * the number of claims.

        I place the first line of the deleted claim on the same level as the last line of the previous claim
        and offset all following lines of the deleted claim by how much above level 2 the last line
        of the previous claim is.

        :param pat_nbr: The patent number.
        :param claims: The list of claims.
        :return: No return needed.
        """
        limit = int(1.2 * len(claims)) + 30
        problematic = list(filter(lambda x: claims[x].id > limit, range(len(claims))))
        problematic.sort(reverse=True)
        for ind in problematic:
            if ind == 0:
                self.logger.warning(f'First claim is unreasonably high: {claims[ind].id}; setting 1', pat_nbr=pat_nbr)
                claims[0].id = 1
                continue
            self.logger.warning(f'Merging unreasonably high claim id {claims[ind].id}.', pat_nbr=pat_nbr)
            lines = claims[ind].lines
            if claims[ind - 1].lines[-1].level == 1:
                lines[0].level = 2
            else:
                lines[0].level = claims[ind - 1].lines[-1].level
                offset = claims[ind -1].lines[-1].level - 2
                for line in lines[1:]:
                    line.level = line.level + offset
            claims[ind - 1].lines.extend(lines)
            del claims[ind]

    def _fix_single_mislabeled(self, pat_nbr: str, claims: List[Claim]) -> bool:
        """
        This function fixes an issue, where a single claim is mislabeled.
        This means that e.g. you have claim 4, 15, 6 and is corrected to 4, 5, 6.
        The corrections are done in place, but info logging events are written.

        :param claims: the claims
        :return: Whether a change was made
        """
        change = False
        for i in range(1, len(claims) - 1):
            guess = claims[i - 1].id + 1
            if claims[i].id != guess and claims[i + 1].id == guess + 1:
                self.logger.info(f'Correction Mislabeled claim {claims[i].id} to {guess}', pat_nbr=pat_nbr)
                claims[i].id = guess
                change = True
        return change

    def _fix_non_splitted(self, pat_nbr: str, claims: List[Claim]) -> bool:
        """
        This tries to fix claims that were not splitted properly.
        This might be more of a job for my claim extraction script
        (as most of these cases appear in the historic files), but I've decided to do it here.
        The main reason for this is that my testing code allows me to quickly evaluate what works.

        I spot these problems by searching for gaps in the claim ids.
        This also gives me the information on which claims are still missing and therefore allows me
        to directly split at the relevant numbers.

        If I've found a claim that is missing, I go through the paragraphs of the previous claim and first
        check, if the digits in the first 5 characters of the text give the searched claim id.
        If they don't, I also look for the typical claim structure ('id.') in the text.

        :param pat_nbr: Patent number.
        :type pat_nbr: str
        :param claims: The list of claims. Altered in place.
        :type claims: List[:class:`Claim`]
        :return: Whether a change was made to the claims.
        """
        change = False
        i = 1
        while i <= len(claims):
            if i < len(claims) and claims[i].id == claims[i-1].id + 1:
                # This claim is alright, continue.
                i += 1
                continue

            # There is a gap here:
            expected = claims[i-1].id + 1
            # Try to split the line at the id
            for j in range(len(claims[i - 1].lines)):
                line = claims[i-1].lines[j]
                # first check the beginning; but only if I'm not in the claims first line.
                if j >= 1 and ''.join(filter(str.isdigit, line.text[:5])) == str(expected):
                    new_claim = Claim(expected, claims[i-1].lines[j:])
                    new_claim.lines[0].level = 1
                    claims.insert(i, new_claim)
                    for k in range(j, len(claims[i-1].lines)):
                        claims[i-1].lines.pop()
                    change = True
                    break
                # then the interior
                elif f'{expected}. ' in claims[i-1].lines[j].text:
                    left, right = line.text.split(f'{expected}. ', 1)
                    # build the new claim and insert it after the current one.
                    right_line = Line(None, 0, 1, f'{expected}. {right}', line.te, line.chem)
                    new_claim = Claim(expected, [right_line] + claims[i-1].lines[(j+1):])
                    claims.insert(i, new_claim)
                    # and remove the information from the old one.
                    for k in range(j + 1, len(claims[i-1].lines)):
                        claims[i-1].lines.pop()
                    line.text = left.strip()
                    line.elem = None
                    # I also want to check if perhaps this line contains no more information. If it doesn't, remove it.
                    if len(line.text) < 5:
                        claims[i-1].lines.pop()
                    change = True
                    break
            i += 1
        if change:
            self._resequence(claims)
        return change

    def _fix_duplicate_claims(self, pat_nbr: str, claims: List[Claim]) -> bool:
        """
        This method checks for duplicate claims.

        As soon as it makes at least one change, the function returns.
        This is the case, since I rely on both missing and duplicate information, which can change
        if I fill in any gaps.

        I add the last claim id + 1 to the missing list, so that a duplicate in the last claim
        can be captured by the first approach.

        :param pat_nbr: The patent number.
        :param claims: The parsed claims list.
        :return: Whether a change was made.
        """
        # first a quick check if there are any duplicate claims. If not, simply return.
        ids = [claim.id for claim in claims]
        if len(ids) == len(set(ids)):
            return False
        # find the duplicate and missing claim ids.
        ids = defaultdict(lambda: [])
        for i in range(len(claims)):
            ids[claims[i].id].append(i)
        duplicates = dict(filter(lambda x: len(x[1]) > 1, ids.items()))
        # I assume that any claim id above the 2*the number of claims is an data error:
        tmp = list(filter(lambda x: x.id < 2 * len(claims), claims))
        missing = [i for i in range(1, max([claim.id for claim in tmp]) + 2) if i not in ids]
        self.logger.warning(f'duplicate claims found: {str(duplicates)} and missing are {str(missing)}',
                             pat_nbr=pat_nbr)
        # now I need to try to fix these.
        for dup_id, dup_indices in duplicates.items():
            if len(dup_indices) > 2:
                self.logger.warning(f'duplicates in more than two positions found: {str(dup_indices)}',
                                     pat_nbr=pat_nbr)
            # first method of correction: look for duplicates that can be put into gaps.
            re_ind = {}
            for dup_ind in dup_indices:
                if dup_ind > 1 and (claims[dup_ind - 1].id + 1) in missing:
                    self.logger.warning(f'resetting id of ind {dup_ind} to {claims[dup_ind - 1].id + 1}',
                                         pat_nbr=pat_nbr)
                    re_ind[dup_ind] = claims[dup_ind - 1].id + 1
            # If I have at least one of the duplicates left, for which I cannot reindex, reindex the others.
            if 0 < len(re_ind) < len(dup_indices):
                for ind, new_id in re_ind.items():
                    claims[ind].id = new_id
                return True  # If I make at least one correction in this step, I need to recalculate everything else.
        for dup_id, dup_indices in duplicates.items():
            # second strategy:
            # First I need to look for unexpected claims and then move them.
            # I need two separate loops, since when I move a claim this changes the indices of other claims.
            unexpected = []
            for dup_ind in dup_indices:
                if (dup_ind == 0 and dup_id != 1) or (dup_id != (claims[dup_ind - 1].id + 1)):
                    unexpected.append(dup_ind)
            if len(unexpected) == 0:
                # There was an edge case where an malformatted patent caused
                # no claim to be unexpected (duplicate claim 1 with a claim zero before
                # the false claim). This caused nothing to be done, but a True to be returned.
                # I need to check for this, if I have no clue simply try the next claim.
                continue
            # move all unexpected claims to the end and then redo the sequences
            unexpected.sort(reverse=True)
            self.logger.warning(f'moving claims at position {unexpected}', pat_nbr=pat_nbr)
            for ind in unexpected:
                claim = claims.pop(ind)
                claim.id = claims[-1].id + 1
                claims.append(claim)
            self._resequence(claims)
            return True
        return False  # I need this final return to trigger in case nothing improved anything.

    def _repair_ids(self, pat_nbr: str, claims: List[Claim]):
        """
        This method calls and manages several methods for repairing the claim ids.

        I do so in a loop, that runs until no function made a change.
        I first look for claims that were not splitted, then look for claims that were simply mislabeled
        and finally I try to fix duplicate claims.
        """
        self._fix_high_claim_ids(pat_nbr, claims)
        change = True
        while change:
            change = self._fix_non_splitted(pat_nbr, claims)
            # Now try to fix a single mislabeled claim
            change = change or self._fix_single_mislabeled(pat_nbr, claims)
            # and ensure that there are no duplicate claims.
            change = change or self._fix_duplicate_claims(pat_nbr, claims)

    @staticmethod
    def _resequence(claims: etree.Element) -> None:
        """Go through the claims and redo the claim sequence."""
        seq = 1
        for claim in claims:
            for line in claim.lines:
                line.sequence = seq
                seq += 1

    def parse_claims(self, claims: etree.Element, pat_nbr: str) -> None:
        """
        The main entry point for parsing the claims.
        This method is called with the claims section as argument,
        cleans it, calls the method to extract the structure and finally
        writes the output to the writer.

        :param claims: The claim section.
        :type claims: :class:`lxml.etree.ElementTree`
        :param pat_nbr: The patent number.
        :type pat_nbr: str
        :rtype: None
        """

        # If working with applications, check the claim reference attributes:
        if self.application:
            self._correct_claim_references(pat_nbr, claims)

        parsed_claims: Dict[str, List[Tuple[int, int]]] = {}
        # each claim is a key, the value if a list of (lvl, seq) tuples.
        # Populated by the rules function.
        self.sequence = 0
        parsed = self._parse_claim_structure(claims)
        self._repair_ids(pat_nbr, parsed)

        for claim in parsed:

            if claim.cancelled:
                parsed_claims[str(claim.id)] = [(1, self.sequence)] # Manually filled, since I never call the rules func
                self.writer.add_line(claim.lines[0].sequence, claim.id, 1, -2, claim.lines[0].text, False, False)
                self.writer.buffer_claim()
                continue

            parsed_claims[str(claim.id)] = []
            for line in claim.lines:
                try:
                    dependency = apply_rules(
                        self.logger, self.parse_keywords, line.elem, line.text,
                        line.level, line.sequence, claim.id, pat_nbr, parsed_claims)
                except KeyError as key:
                    self.logger.warning('Key {} not found in patent, setting -1'.format(str(key)), pat_nbr=pat_nbr)
                    dependency = -1
                    parsed_claims[str(claim.id)].append((line.level, line.sequence))
                self.writer.add_line(
                    line.sequence, claim.id, line.level, dependency, line.text, line.te, line.chem)
            self.writer.buffer_claim()
        self.writer.write_patent(pat_nbr)


class ParseV2(ParseXML):

    parse_keywords = {
        'pat_tag': 'PATDOC', 'tag_claim_list': 'SDOCL/CL',
        'tag_single_claim': 'CLM', 'attr_claim_id': 'ID',
        'expected_tags_in_claim': ['PARA', 'CLMSTEP', 'PTEXT'],
        'level_key': 'LVL', 'reference_tag': 'CLREF', 'attr_ref_id': 'ID',
        'keyword_clmstep': 'CLMSTEP', 'keywords_te': ["TABLE-US", "MATH-US"],
        'ptext': 'PTEXT', 'keywords_chem': ['CHEM-US']
    }
    # the PTEXT tag requires special treatment,
    # since it is used only for tables, etc.

    patnbr_tags = ['SDOBI', 'B100', 'B110']
    reg = re.compile(r'<\?xml version="1\.0" encoding="UTF-8"\?>\n<!DOCTYPE PATDOC SYSTEM "[0-9A-Za-z\.-]*" \[ ?(\n<!ENTITY [a-zA-Z0-9\"\. -]*>)*\n?\]>')

    def __init__(self, writer, application):
        if application:
            sys.exit('applications not allowed for Version2')
        super().__init__(writer)
        self.logger = logging.getLogger('ParseV2')

    def _get_patnbr(self, root):
        """
        Find the patent number
        """
        pat_nbr = ''.join(
            [x.text.strip() if x.text is not None else ''
             for x in root.find(self.patnbr_tags[0]).find(
                self.patnbr_tags[1]).find(self.patnbr_tags[2]).iter()])
        if pat_nbr == '':
            self.logger.error('Bad patent number!')
            return None
        return pat_nbr

    def parse_claims(self, claims, pat_nbr):
        """Parsing the claims.

        No return, output is written to file"""
        parsed_claims = {}
        sequence = 0

        for claim in claims.iter(self.parse_keywords['tag_single_claim']):
            claim_id = int(claim.get(self.parse_keywords['attr_claim_id'])[4:])
            parsed_claims[str(claim_id)] = []
            first_line_in_claim = True

            for line in claim:
                # first throw out weird and unexpected lines.
                if line.tag not in self.parse_keywords['expected_tags_in_claim']:
                    self.logger.warning('weird tag found in claim {}'.format(line.tag), pat_nbr=pat_nbr)
                    continue
                sequence += 1

                (TE, CHEM) = check_TEC(line, self.parse_keywords)
                text = to_text(line)
                if line.tag == self.parse_keywords['ptext']:
                    self.writer.add_line(
                        sequence, claim_id, 101, sequence - 1, text, TE, CHEM)
                    parsed_claims[str(claim_id)].append((101, sequence))
                    self.logger.debug(
                        "rule l1 wrote output line {}".format(sequence), pat_nbr=pat_nbr)
                    continue

                try:
                    level = int(line.get('LVL'))
                except ValueError:
                    self.logger.error(
                        "value error getting level line {}".format(sequence), pat_nbr=pat_nbr)
                    continue
                # level 0 is level 1
                if level == 0:
                    level = 1
                # level 7 is level 1
                if level == 7 and first_line_in_claim:
                    level = 1
                first_line_in_claim = False
                if line.tag == self.parse_keywords['keyword_clmstep'] and level == 1:
                    level = 2

                try:
                    dependency = apply_rules(
                        self.logger, self.parse_keywords, line, text,
                        level, sequence, claim_id, pat_nbr, parsed_claims)
                except KeyError as key:
                    self.logger.warning('Key {} not found in patent, setting -1'.format(str(key)), pat_nbr=pat_nbr)
                    dependency = -1
                    parsed_claims[str(claim_id)].append((level, sequence))
                self.writer.add_line(sequence, claim_id, level, dependency, text, TE, CHEM)

            self.writer.buffer_claim()
        self.writer.write_patent(pat_nbr)


class ParseV1(ParseV4):
    """
    This is a parser working solely on the patent application xml format 1.6.
    It is virtually identical to the V4 parser, except that some keywords are different.

    Therefore, I subclass the V4 parser, provide different keywords,
    and switch of the keyword changing behaviour in the V4 init.
    """

    parse_keywords = {
        'pat_tag': 'patent-application-publication', 'tag_claim_list': 'subdoc-claims',
        'tag_single_claim': 'claim', 'attr_claim_id': 'id',
        'keywords_chem': ['chemistry', 'us-chemistry'],
        'tag_claim_text': 'claim-text', 'xp_claim_par': './claim-text',
        'reference_tag': 'dependent-claim-reference', 'attr_ref_id': 'depends_on',
        'keywords_te': ["us-math", "tables", 'maths']
    }

    patnbr_tags = [
        'subdoc-bibliographic-information',
        'document-id', 'doc-number'
    ]
    reg = re.compile(r'<\?xml version="1\.0" encoding="UTF-8"\?>\n<!DOCTYPE .* \[\n?(<!ENTITY .*>\n)*\n?\]>\n?')

    def __init__(self, writer, application):
        """Even when I parse applications, I don't have to pass the application flag to the V4 parser.
        The in this version seem to be the same for both applications and patents."""
        super().__init__(writer, application=True, no_change_keywords=True)

    def _get_patnbr(self, root) -> str:
        """
        Can be simplified to a few lines.
        :param root: The patent root as xml document
        :return: :class:`lxml.etree.Element`
        :return: The patent number on success, None on failure.
        :rtype: str or None
        """
        r1 = root.find(self.patnbr_tags[0])
        r2 = r1.find(self.patnbr_tags[1])
        pat_nbr = r2.find(self.patnbr_tags[2]).text
        return pat_nbr


class ParseHistoric(ParseV4):

    def __init__(self, writer, application=False):
        """Application parsing is not supported, but I need the flag to be here anyway."""
        super().__init__(writer, application=True, no_change_keywords=True)
        self.logger = logging.getLogger('ParseHistoric')
        self.parse_keywords['pat_tag'] = 'patent'
        self.memfile = None

        self._claim_extractor = src.extraction.Extraction()
        self.file = ''
        self._reader = None
        self._description = ''

    def __del__(self):
        if self.memfile:
            self.memfile.close()

    def open(self, file: str) -> bool:
        self.file = file
        self._reader = src.read_historic.read(self.file)
        return True

    def __iter__(self):
        return self

    def __next__(self) -> Tuple[Optional[etree.Element], Optional[str]]:
        try:
            pat_nbr, pat_str = next(self._reader)
            if pat_str is None:
                return None, None
            pat = etree.fromstring(pat_str)
        except StopIteration:
            raise StopIteration
        except etree.LxmlError as err:
            self.logger.error(err, pat_nbr=pat_nbr)
            return None, None
        return pat, pat_nbr

    def _get_patnbr(self, root) -> Optional[str]:
        return root.get('pat_nbr', None)

    def claim_id(self, claim) -> int:
        return int(claim.get("claim-id"))

    def parse_claims(self, patent_str: etree.Element, pat_nbr: str) -> None:
        """
        I need to overwrite the V4 function here, since here I iterate over the patents and not just the claims.
        I simply extrac the claims from the patents and then call the super function with that.

        :param patent_str: The patent element given from the iterator.
        :param pat_nbr: The patent number.
        """

        patent = self._claim_extractor.parse(pat_nbr, patent_str)
        if patent is None:
            return
        claims = patent.find('claims')
        ParseV4.parse_claims(self, claims, pat_nbr)


class ParseCanadian(ParseV4):
    """
    This class parses canadian patents.
    Since these do not come in a well-prepared xml format,
    I manually created the xml elements.

    Therefore, I need to rework the open function to work with the zipfile.

    Since I'm not 100% sure about the quality of my splits,
    I have enabled the error parsing functionalities for applications.
    """

    parse_keywords = {
        "pat_tag": "patent",
        "tag_claim_list": "claims",
        "tag_single_claim": "claim",
        "attr_claim_id": "id",
        "tag_claim_text": "claim-text",
        "xp_claim_par": "./claim-text",
        "reference_tag": "NULL",
        "attr_ref_id": "NULL",
        "keywords_te": [],
        "keywords_chem": [],
    }

    def __init__(self, writer):
        super().__init__(writer=writer, application=True, no_change_keywords=True)

    def open(self, file: str) -> bool:
        """
        The path provided links to the zipfile.
        I need to open this, and store the file in it as memfile.

        :param file: The path to the zipfile to open.
        :return: A boolean indicating success.
        """
        print(f"opening file {file}")
        self._zfile = ZipFile(file)
        self.memfile = self._zfile.open("canada.xml")
        return True

    def _get_patnbr(self, root) -> Optional[str]:
        return root.get("pat_nbr")