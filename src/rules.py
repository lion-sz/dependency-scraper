from typing import List

"""
This module contains the functions that apply the rules. Most importantly the
function apply_rules, but also two smaller function, check_TEC and clean_word.

Therefore - if there is a problem with the classification - the relevant code
is the function apply_rules from this module.
"""

spellings_claim = [
    ' claim ', ' claims ', ' claimed in ', ' recited in ',
    ' claimed ',
    ' claim,', 'claims,', 'claim.', 'claims.'
]
"""
If any of these words is in a line it is assumed
to contain a reference to an other claim.
It is then checked if the next 5 words contain a number.
If they do this is the dependency.
"""


keywords_preceding_claim = [
    'preceding claim', 'previous claim',
    'aforementioned claim', 'claimed earlier',
    'claimed above', 'claimed previously']
"""
If one of these keywords is met the referred claim is the last claim.
"""


keywords_preceding_claims = [
    'preceding claims', 'previous claims',
    'any one of claims', 'any preceding claim',
]
"""
If any of these keywords is found there are several earlier
claims referenced.
In this case the dependency is the last claim.
"""


unit_chars = ['%', '(', ')', '$']
separators = ['-', ',']


def check_TEC(line, parse_keywords):
    """
    This function checks if a line contains a table or equation.
    This is done using the keywords passed on from the parsers.

    :param line: The line as an xml element.
    :type line: class:`lxml.etree.Element`
    :param parse_keywords: The dictionary of keywords.
        It must contain two elements 'keywords_te' and 'keywords_chem'
        that should refer to lists
    :type parse_keywords: Dict
    :rtype: (bool, bool)
    """
    TE = False
    CHEM = False
    for child in line.iter():
        if child.tag in parse_keywords['keywords_te']:
            TE = True
        if child.tag in parse_keywords['keywords_chem']:
            CHEM = True
    return TE, CHEM


def clean_word(word: str) -> str:
    """This function simply takes a word that might be a reference to a claim
    and removes all non digits from it.

    Even with whitespaces before and after, punctuation,
    etc. a word should not be longer than 5 chars. Therefore, everything with
    more than 5 characters is omitted.

    If there are two numbers separated by any character in the `separators` array,
    the function stops and returns the parts before the separator.

    The characters in the `unit_chars` array often indicate that a number is a not a claim reference.
    Therefore, when encountering them, the function stops and returns an empty sting.

    :param word: The string that should be cleaned.
    :type word: str
    :rtype: str
    """

    # Even with punctuation, etc. a word should not be longer than 5 chars
    if len(word) > 5:
        return ''
    tmp = []
    for d in word:
        if d.isdigit():
            tmp.append(d)
        elif d in unit_chars:
            return ''
        elif d in separators and len(tmp) != 0:
            return ''.join(tmp)
    return ''.join(tmp)


def apply_rules(logger, parse_keywords, line, lower_text, level, sequence, claim_id, pat_nbr, parsed_claims):
    """
    This is the only function that applies the rules. Therefore the entire code is one place.

    Right now the code always returns a dependency.

    The code gives precedence to xml claimref tags over just plain text claim references.

    This function applies all rules and therefore is rather long.
    Furthermore it was written and tested in the first working version and
    since then I did not rewrite / refactor this code it is unreadable.
    The lines are to long, there is commented out code and so on.
    So when looking through this code, good luck!

    Returns the dependency - int.

    :param logger: The logger of the calling class.
    :type logger: class:`logging.Logger`
    :param parse_keywords: The dictionary of xml keywords from the calling class.
        The required elements are 'reference_tag', 'attr_ref_id'.
    :type parse_keywords: Dict
    :param line: The line as xml element. This is marked as a required
        parameter, but if given None the code simply omits checks based
        on the xml and just uses the text.
    :type line: class:`lxml.etree.Element` or None
    :param lower_text: The text contained in a line, casted to all lower case.
        As opposed to line this has to be provided.
    :param level: The level of the claim line.
    :type level: int
    :param sequence: The sequence number of the claim line
        within the claims section.
    :type sequence: int
    :param claim_id: The id of the claim within the claim section.
    :type claim_id: int
    :type pat_nbr: The patent number. This is used when writing warnings
        or errors to the logger.
    :type pat_nbr: str
    :param parsed_claims: The Dictionary containing the claim structure.
        This is essential since it is used to look up the sequence of a
        referred claim.
    :type parsed_claims: Dict
    :rtype: int
    """
    dependency = -1

    # Step 1: The first claim is always independent.
    if sequence == 1:
        parsed_claims[str(claim_id)].append((level, sequence))
        return 0

    # Step 2: Claims with a level >= 2
    # Since for each claim the first paragraph is always level 1, I only check the current claim.
    if level >= 2:
        # I assume that the claim_id is already in the parsed claims. If this is not the case, the program will crash.
        # This is desired, as this error should NEVER happen.
        # iterate backwards through this claim
        for i in range(len(parsed_claims[str(claim_id)])):
            if parsed_claims[str(claim_id)][-(i + 1)][0] < level:
                # get the first sequence with a smaller level
                dependency = parsed_claims[str(claim_id)][-(i + 1)][1]
                break
        # append to parsed claims and return dependency.
        parsed_claims[str(claim_id)].append((level, sequence))
        return dependency

    # Now I know the level to be 1 and can search for a claim reference.

    # Step 3: look for XML references. Only, if a line was provided.
    referred_claim = None
    if line is not None:
        # first check for reference tags
        for child in line.iter(parse_keywords['reference_tag']):
            ref = int(child.get(parse_keywords['attr_ref_id'])[4:])
            if referred_claim is None or ref < referred_claim:
                referred_claim = ref

    # Step 4: Look for a general text reference.
    if referred_claim is None:
        for key in spellings_claim:
            if key in lower_text:
                referred_claim = lower_text.split(key, 1)[1]
                break

    # Step 5: Independent claims. If there is no reference to another claim, this is independent.
    if referred_claim is None:
        parsed_claims[str(claim_id)].append((level, sequence))
        return 0

    # Step 6: Parsing Text references. Only triggers, if I didn't yet find an numeric reference.
    # First I look for an explicit reference in the first 5 words:
    if type(referred_claim) is str:
        # get only the first 5 words.
        words = referred_claim.strip().split(' ', 5)
        if len(words) > 5:
            words = words[:5]
        # go through all words and try to get
        matches: List[int] = []
        for word in words:
            if clean_word(word) == '':
                continue
            matches.append(int(clean_word(word)))
        if len(matches) > 0:
            referred_claim = min(matches)

    # Step 7: Interpreting numeric references.
    # This step will return a value.
    if type(referred_claim) is int:
        # correct references
        if claim_id > referred_claim > 0:
            try:
                dependency = parsed_claims[str(referred_claim)][0][1]
            except IndexError:
                logger.critical(f'referred claim {referred_claim} has no information in parsed claims.', pat_nbr=pat_nbr)
        elif referred_claim <= 0:
            logger.warning(f'found reference to claim {referred_claim}', pat_nbr=pat_nbr)
        elif referred_claim >= claim_id:
            # A claim that references either a later claim or itself.
            # If the exception to rule 8 holds, assign the last claim as dependency, if not keep -1.
            if str(claim_id - 1) in str(referred_claim):
                dependency = parsed_claims[str(claim_id - 1)][0][1]
        parsed_claims[str(claim_id)].append((level, sequence))
        return dependency

    # Step 8: Preceding Claims
    # first check for one preceding claim
    try:
        for key in keywords_preceding_claim:
            if key in lower_text:
                parsed_claims[str(claim_id)].append((level, sequence))
                return parsed_claims[str(claim_id - 1)][0][1]
        # check for several preceding claims:
        for key in keywords_preceding_claims:
            if key in lower_text:
                parsed_claims[str(claim_id)].append((level, sequence))
                return parsed_claims[str(claim_id - 1)][0][1]
    except IndexError as err:
        logger.error(f'Claim {str(claim_id - 1)} not in patent.', pat_nbr=pat_nbr)
        return -1

    # Step 9: Unparsable claims.
    dependency = 1
    parsed_claims[str(claim_id)].append((level, sequence))
    return dependency
