"""
This module manages the downloading and unzipping of the data files.
This is done through the Bulk class.
"""

import bs4
import urllib.request
import zipfile
import logging
import pathlib

# The Link containing the list of all the zip files is BASELINK_ZIPLIST + the year
BASELINK_GRANT = "https://bulkdata.uspto.gov/data/patent/grant/redbook/fulltext/"
BASELINK_APPLICATION = "https://bulkdata.uspto.gov/data/patent/application/redbook/fulltext/"


class Bulk:
    """
    This class manages my downloaded copies of the bulkdata.
    It is also the only class that needs a network connection.
    If this script should be adopted to work without a network this would be the class to alter.

    For every year it first goes to the bulkdata site to get information
    on which years it should find.
    The main can then iterate through the links and download / extract
    every file.

    By default the extracted files are deleted and only the archived files are kept.

    This class provides an iterator that allows to iterate through the links.
    But note that for every link the prepare_xml function must be called.

    :param allow_offline: If true this parameter enables the usage of
        already downloaded files and keeps downloaded files.
    :type allow_offline: bool, optional
    :param data_path: The path where the downloaded zipfiles should be stored
    :type data_path: str, optional
    :param tmp_path: The path where the extracted xml files are stored.
        This parameter is important if the datapath is a network drive
        since local disks are typically much faster.
    :type tmp_path: str, optional
    :param expand_path: This parameter enables the expansion of data paths
            into $year(_xml) subpaths. This helps to structure the data files.
    :type expand_path: bool, optional
    :param update: When the bulkdata is build in the update mode it will skip
        all already existing files.
        This alters the behaviour of the prepare xml function.
        It returns None for all already existing files.
    :type update: bool, optional
    :param force_reload: Always fetch the list of urls from the USPTO.
        Save option, therefore enabled by default.
    :type force_reload: bool
    :param applications: If True the bulkdata parser will download patent applications.
    :type applications: bool
    """

    def __init__(
            self, allow_offline=True, data_path='./', tmp_path=None,
            expand_path=True, update=False, applications=False, force_reload=True):
        self.allow_offline = allow_offline
        self.expand = expand_path
        self.data_path = pathlib.Path(data_path)
        self.tmp_path = pathlib.Path(tmp_path) if tmp_path is not None else None
        self.update = update
        self.force_reload = force_reload
        self.expand_path = ''
        self.logger = logging.getLogger('Bulk')
        self.ft = ''
        self._year = ''
        self._pos = 0
        self.baselink = BASELINK_GRANT if not applications else BASELINK_APPLICATION

    def __iter__(self):
        self._pos = -1
        return self

    def __next__(self):
        self._pos += 1
        if self._pos >= len(self.zip_list):
            raise StopIteration
        else:
            return self.prepare_xml(self.zip_list[self._pos])

    def open_year(self, year: int):
        """
        This function prepares a year by first downloading the corresponding file from
        the uspto and then parsing all links to the weekly zipfiles from it.

        The names of the zipfiles are then stored in a class attribute and given back
        through the iterator.

        This method requires a network connection, but could be edited to read the
        list of files from a folder.

        :param year: The year that should be prepared.
        :type year: int

        :rtype: None
        """
        self._year = str(year)
        self.ft = '.xml' if year >= 2002 else '.txt'

        if self.expand:
            self.expand_path = self._year + ('_xml/' if year >= 2002 else '/')
            # check if this path exists in filesystem, if not create it.
            self.year_folder = self.data_path / self.expand_path
        else:
            self.year_folder = self.data_path
        self.year_folder.mkdir(exist_ok=True)

        if not self.force_reload:
            zip_cont = list(self.year_folder.glob('*.zip'))
            expected_len = [51, 52, 53] if year >= 1976 else [3, 4]
            if len(zip_cont) in expected_len:
                self.logger.info('{} already has {} zipfiles, not fetching list'.format(year, len(zip_cont)))
                self.zip_list = [z.name for z in zip_cont]
                return None

        # try to get the list of relevant files
        try:
            res = urllib.request.urlopen(self.baselink + self._year).read()
            soup = bs4.BeautifulSoup(res, features='lxml')
        except Exception as err:
            # if this call fails simply set ziplist to []
            # therefore the main parser will not receive any files
            self.logger.critical('Failed finding relevant files:')
            self.logger.critical(err)
            self.zip_list = []
            return None

        links = soup.find_all('a')
        self.zip_list = []
        for link in links:
            if '.zip' in link.get('href'):
                self.zip_list.append(link.get('href'))

        # The year 2001 is a special case; there are pg and pftaps files, select only pftaps
        if year == 2001:
            self.zip_list = [l for l in self.zip_list if 'pftaps2001' in l]

    def prepare_xml(self, link):
        """
        The function takes a filename as provided by this classes iterator and
        - if needed - downloads the corresponding zipfile.

        The local file is then extracted and the resulting xml file is stored in
        either the tmp path or the datapath.

        Returns a tuple of the zipfile and xml file path.
        On failure returns (None, None).

        If the update mode is set and the file already exists the return will
        also be (None, None).

        :param link: The filename as returned by this classes iterator.
        :type link: str
        :return: The paths of the zipfile and the xml file. On error None.
        :rtype: Tuple[:class:`pathlib.Path`, :class:`pathlib.Path`] or Tuple[None, None]
        """
        # Download the file if necessary
        filepath = self.year_folder / link
        # in update mode check if the file exists and if so return (None, None)
        if self.update and filepath.is_file():
            return None, None
        # first check if the file does not exists or I want to stay offline
        if not filepath.is_file() or not self.allow_offline:
            try:
                # attempt to download and write the file
                res = urllib.request.urlopen(
                    self.baselink + self._year + '/' + link).read()
                filepath.write_bytes(res)
            except Exception as err:
                # download failed - since file not presen abort!
                self.logger.error('Error downloading file: ' + str(err))
                return None, None

        # Extract the zipfile
        target_path = self.year_folder if self.tmp_path is None else self.tmp_path
        try:
            with zipfile.ZipFile(filepath, 'r') as zfile:
                zfile.extractall(path=target_path)
        except Exception as err:
            self.logger.error('Failed extracting zipfile' + str(err))
            return None, None

        # first test the link since one zip file has a .XML file in it
        target_file = target_path / link.replace('.zip', self.ft)
        if not target_file.is_file():
            # try to find a .XML file
            if (target_path / link.replace('.zip', '.XML')).is_file():
                (target_path / link.replace('.zip', '.XML')).rename(target_file)
            else:
                self.logger.error('failed finding extracted data: {}'.format(link))
                return None, None

        return filepath, target_file

    def cleanup(self, zip_path, xml_path):
        """
        This method is supposed to be called once the parsing is done.
        It deletes the xml file and if needed the zip file.

        :param zip_path: The filepath of the zipfile.
        :type zip_path: str
        :param xml_path: The filepath of the xml file.
        :type xml_path: str
        :rtype: None
        """
        xml_path.unlink()
        if self.allow_offline is False:
            zip_path.unlink()
        return None
