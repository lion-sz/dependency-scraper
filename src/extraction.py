from typing import Generator, Tuple, Optional, List
import lxml.etree as etree
import re
import logging
from collections import namedtuple


Paragraph = namedtuple('Paragraph', ['text', 'lvl'])
Claim = namedtuple('Claim', ['id', 'paragraphs'])
Claims = namedtuple('Claims', ['pat_nbr', 'claims'])


class Extraction:
    """
    This class manages the code to extract the claim segment from historic patents.
    The claims are then formatted like the USPTO XML Version 4,
    except that it does not contain any claim reference tags.

    To do so, I check three strategies for finding the claims:
    1. Claim Statement
        I go through all paragraphs and check if they contain a claim statement
        ('i claim', 'iclaim', 'we claim', 'is claimed').
        If I find several statements, I use the last one that has at least 5 paragraphs behind it.
        I then parse the structure from the claim ids following the statement.
    2. Claim ID
        I start from the last paragraph and look through the next eight paragraphs for any claim id.
        If I find a claim id, I continue and check the next five paragraphs for a new id.
    3. One Claim
        Here I check for a claim statement that is not followed by proper claim ids.
        I then impose that that everything is one claim.
    """
    claim_statements = [
        'i claim', 'iclaim',
        'we claim', 'is claimed'
    ]

    _claim_id_reg = re.compile(r'^([0-9]+)\.[ a-zA-Z]')
    _claim_id_internal_reg = re.compile(r'^.*( [0-9]+\. .*)$')
    _claim_id_special_reg = re.compile(r'^[a-zA-Z ]{1,3}([0-9]+)\.[ a-zA-Z]')
    _chars_replace = [',', ';', '\'', '-']
    _to_remove = ['references cited', 'testimony',
                  'united states patent', 'commissioner of patents',
                  'witness', 'vitness', 'witnesses', 'vitnesses',
                  'examiner']
    _xp_following = etree.XPath('following-sibling::p')
    _xp_preceding = etree.XPath('preceding-sibling::p')

    def __init__(self):
        self.logger = logging.getLogger('Extractor')

    def _extract_statement(self, pat_nbr: str, description: etree.Element) -> Optional[etree.Element]:
        """
        Try to find the start of the claims section through the claim statement.
        I check every statement in the list.

        Furthermore I test, if I can find a claim number in the statement paragraph.
        If so, I split beginning at that number and return all following siblings.

        If I encounter several claim statements, I always use the last one,
        given that it has at least 5 following paragraphs. If this does not work, I use the first one.
        This rule is in place, since some patents contain a claim statement in the
        tailing paragraphs.

        To properly parse the claim structure, I rely on claim ids.
        If the patent does not use recognizable claim ids, this function might not be able to parse it.
        For those cases the one statement function might still be able to extract information.

        :param description: The description as lxml Element
        :return: All siblings following the claim statement and all siblings preceding it.
        """
        found: List[etree.Element] = []
        for child in description:
            ct = child.text.strip().lower()
            # check if the claim started.
            contains, right = self._contains_stm(ct)
            if contains:
                found.append((child, right))

        if len(found) == 0:
            return None
        stm, right = None, None
        for i in range(len(found)):
            stm, right = found[-i]
            if len(self._xp_following(stm)) < 5:
                continue
        if stm is None:
            stm, right = found[0]
        claims = self._xp_following(stm)
        if self._claim_id_internal_reg.match(right) is not None:
            elem = etree.Element('p')
            elem.text = self._claim_id_internal_reg.match(right).groups()[0]
            claims.insert(0, elem)

        # get the preceeding claims:
        desc = self._xp_preceding(stm)
        desc.insert(0, stm)
        return self._format(pat_nbr, claims, desc)

    def _extract_ids(self, pat_nbr, description: etree.Element) -> Optional[etree.Element]:
        """
        The second strategy is supposed to find claims that have no or an unrecognizable claim statement.
        It works by looking from the bottom up for claim ids. If I find a claim id in the first 8 paragraphs from the
        bottom, I continue from this claim and check the next five paragraphs for another.
        Thus, I work my way up from the bottom of the patent, hopefully finding the claims.

        If I've found any claims I also check the two preceding paragraphs for internal claims or 'l.' claim ids.
        This is ment to capture an internal claim with an unrecognizable claim statement.
        """
        children = description.getchildren()
        children.reverse()

        last_seq = -1
        for i in range(len(children)):
            if i - last_seq > 5:
                break
            if self._claim_id(children[i].text.lower().strip()) is not None:
                last_seq = i

        if last_seq == -1:
            return None
        # build the claims interval
        claims = [children[i] for i in range(last_seq + 1)]
        claims.reverse()
        # Check the two preceding claims I'm not sure, how much of these cases are ever triggered,
        # but only if there are at least three more paragraphs.
        if len(children) - last_seq <= 2:
            pass
        elif self._claim_id_internal_reg.match(children[last_seq + 1].text.lower().strip()) is not None:
            claims.insert(0, children[last_seq + 1])
            claims[0].text = self._claim_id_internal_reg.match(claims[0].text.lower().strip()).groups()[0]
        elif self._claim_id_internal_reg.match(children[last_seq + 2].text.lower().strip()) is not None:
            claims.insert(0, children[last_seq + 1])
            claims.insert(0, children[last_seq + 2])
            claims[0].text = self._claim_id_internal_reg.match(claims[0].text.lower().strip()).groups()[0]

        # get everything before the first claim as a description
        desc = self._xp_preceding(claims[0])
        return self._format(pat_nbr, claims, desc)

    def _extract_one_claim(self, pat_nbr: str, desc: etree.Element) -> Optional[etree.Element]:
        """
        This method assumes that there is only one claim and that it is preceded by a claim statement.
        It is supposed to run when there are no claim ids in the following paragraphs.
        I check only the last 5 paragraphs.
        All paragraphs following the claim statement are one claim.

        For this to work, I need to make sure that there is a claim number. If I was not successful at parsing one,
        I simply insert a 1. to the first claim.
        :param desc: The description
        :return:
        """
        children = desc.getchildren()
        following = None
        stm = None
        for i in range(1, min(10, len(children)) + 1):
            ct = children[-i].text.lower().strip()
            contains, right = self._contains_stm(ct)
            if contains:
                stm = right
                following = self._xp_following(children[-i])
                desc = self._xp_preceding(children[-i])
                break

        if stm is None:
            return None

        # Check for a few internal claim cases.
        if self._claim_id_internal_reg.match(stm) is not None:
            elem = etree.Element('p')
            elem.text = self._claim_id_internal_reg.match(stm).groups()[0]
            following.insert(0, elem)
        elif '-' in stm[:-1]:
            # Some claims are separated with this character.
            elem = etree.Element('p')
            elem.text = stm.split('-', 1)[1]
            following.insert(0, elem)
        elif ':' in stm[:-1]:  # I don't want to recognize a trailing ':' as a separator.
            elem = etree.Element('p')
            elem.text = stm.split(':', 1)[1].strip()
            if len(elem.text) >= 3:  # Again, I don't want to add any useless stuff.
                following.insert(0, elem)

        if len(following) == 0:
            return None
        # Make sure that there is a claim id.
        if self._claim_id(following[0].text) is None:
            following[0].text = '1. ' + following[0].text
        return self._format(pat_nbr, following, desc)

    @staticmethod
    def _check_l(elem: etree.Element):
        """This function checks if there is an l or an I in the first 3 characters of the claim.
        Since I don't want to change these letters in normal words, I also check, that the first 5
        characters contain at least a number or a '.' or ';'.

        If both conditions are met, change that to a 1.
        """
        if len(elem.text.strip()) <= 4:
            return
        if 'l' in elem.text[:3] or 'I' in elem.text[:3]:
            if '.' in elem.text[:3] or ';' in elem.text[:3] or \
                    len(list(filter(str.isdigit, elem.text[:3]))) > 0:
                first = elem.text[0:3].replace('l', '1').replace('I', '1')
                elem.text = first + elem.text[3:]

    def parse(self, pat_nbr: str, root: etree.Element) -> Optional[etree.Element]:
        """
        Parse the actual structure and write the output to the output file.

        I detect claims by iterating through the paragraphs and looking for a claim statement.
        I then assume that all lines following the claim statement are claims.
        Each line can be either a new claim or a lvl 2 claim paragraph,
        based on whether I can parse a claim number.
        The last claim might have a few tailing paragraphs,
        containing names / references cited and other stuff.
        I therefore check, if I need to remove them.

        A claims section might be missing the statement and thus I should be capable of
        detecting claims even without a statement.
        :return: A boolean indicating if the parsing was successful.
        """
        # TODO 2021-03-13 lion: Detection with incomplete claim statements
        # TODO 2021-03-14 lion: Split a paragraph after the claim statement.
        # TODO 2021-03-14 lion: Improve the detection of references cited.
        self._flag_tailing(root)
        tail = etree.Element('tail')
        for elem in root.xpath('.//p[@remove="true"]'):
            root.remove(elem)
            tail.append(elem)

        # Look for the l and I data errors:
        for elem in root.xpath('.//p'):
            self._check_l(elem)

        # Check for statement based methods.
        patent = self._extract_statement(pat_nbr, root)
        # Work with the id based method
        if patent is None:
            self._extract_ids(pat_nbr, root)
        # And finally the one line based method.
        if patent is None:
            patent = self._extract_one_claim(pat_nbr, root)

        # append the tail to the patent
        if patent is not None:
            patent.append(tail)
        return patent

    def _format(self, pat_nbr: str, claims_list: List[etree.Element], desc: List[etree.Element]) -> Optional[etree.Element]:
        """
        Format the claims list and description xml Element representing the patent.

        :param claims_list: A list of XML elements, without level claim id information.
        :param desc: A list of XML elements, that are written into the description.
        :return: The finished  patent element.
        :rtype: :class:`etree.Element`
        """
        # first format the claims
        claims = etree.Element('claims')
        claim = None
        last_lvl1 = None
        for child in claims_list:
            ct = child.text
            # If the claim started, add this to a new claim
            claim_id = self._claim_id(ct)
            if claim_id is not None:
                claim = etree.Element('claim', attrib={'claim-id': claim_id})
                claims.append(claim)
                last_lvl1 = etree.Element('claim-text', attrib={'lvl': '1'})
                last_lvl1.text = ct
                claim.append(last_lvl1)
            else:
                if last_lvl1 is not None:
                    tmp = etree.Element('claim-text', attrib={'lvl': '2'})
                    tmp.text = ct
                    last_lvl1.append(tmp)
        if claim is not None:
            claims.append(claim)
        if len(claims.findall('claim')) == 0:
            return None
        claims.set('id', 'claims')

        # next the description
        description = etree.Element('description')
        for p in desc:
            description.append(p)
        # and the patent element
        patent = etree.Element('patent', attrib={'pat_nbr': pat_nbr})
        patent.append(description)
        patent.append(claims)
        return patent

    def _flag_tailing(self, desc: etree.Element) -> None:
        """
        Remove all non-informative descriptions paragraphs from the end.

        There are three cases in which I remove a line:
            * Less or equal to six words or 50 characters, therefore likely a name.
                But I do check if this is a claim statement, as they sometimes are short.
            * it contains any of the words to remove.

        I iterate through the paragraphs from the end and check three paragraphs.
        Furthermore, If the last element was removed, i also check the next one
        (but I'm stricter at the length rule, 3 and 30 and only use the words
        if there are at most 150 characters).

        :param desc: The description element.
        :return: The description is changed and therefore no return.
        """
        last_removed = False
        for i in range(1, min(4, len(desc.getchildren())) + 1):
            last_removed = False
            ct = desc[-i].text.lower().strip()
            # Case 1: Name, at most three words or 25 characters
            if (len(ct.split()) <= 6 or len(ct) <= 50) and not self._contains_stm(ct)[0]:
                desc[-i].set('remove', 'true')
                last_removed = True
            # Case 2: the words to remove
            else:
                for reg in self._to_remove:
                    if reg in ct:
                        last_removed = True
                        desc[-i].set('remove', 'true')
                        break
        i = 4
        num_children = len(desc.getchildren())
        while last_removed:
            last_removed = False
            i = i + 1
            if i >= num_children + 1:
                break
            ct = desc[-i].text.lower().strip()
            if len(ct) > 150:
                break
            if len(ct.split()) <= 3 or len(ct) <= 30 and not self._contains_stm(ct)[0]:
                desc[-i].set('remove', 'true')
                last_removed = True
            else:
                for expr in self._to_remove:
                    if expr in ct:
                        last_removed = True
                        desc[-i].set('remove', 'true')
                        break

    def _contains_stm(self, text: str) -> Tuple[bool, Optional[str]]:
        """
        Check if a element contains a claim statement.
        If it does, return the text to the right of the first statement found.

        :param text: The text of the element that should be checked.
        :return: A boolean whether the text contains a statement and if it does the text to the right of it.
        """
        for stm in self.claim_statements:
            if stm in text:
                return True, text.split(stm, 1)[1].strip()
        return False, None

    def _claim_id(self, text: str) -> Optional[str]:
        """
        This function tries to extract a claim id from the claim text.

        The base pattern is a number followed by a dot and space, though I also allow for an alphabetic character.
        There are a few conditions that I fix:
            * If there are up to three alphabetical characters before the pattern, I remove these.
            * An '1' might be misread as a 'l', therefore I replace any 'l' with '1' in the first 8 chars
                and try the default patent.
            * Furthermore, I remove some special characters from the string.

        :param text: The claim text, casted to lower.
        :return: The claim id as string, None if not found.
        """
        text = text.lower().strip()
        id = None
        if self._claim_id_reg.match(text) is not None:
            id = self._claim_id_reg.match(text).groups()[0]

        if id is None and self._claim_id_special_reg.match(text) is not None:
            id = self._claim_id_special_reg.match(text).groups()[0]

        # Here I try cleaning the first 5 characters.
        if id is None:
            start = text[:5]
            for char in self._chars_replace:
                if char in start:
                    start = start.replace(char, '').strip()
            if len(start) == 0:
                self.logger.warning(f'empty start looking for claim id: {text}.')
                return None
            if start[0] == '.':
                start = start[1:].strip()

            if self._claim_id_reg.match(start) is not None:
                id = self._claim_id_reg.match(start).groups()[0]
        if id is None or int(id) <= 0:
            return None
        else:
            return id
