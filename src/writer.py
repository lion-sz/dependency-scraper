"""
This module contains a class to write the claims to disk.
"""

import csv
import pathlib


class Writer:
    """
    The writer is manages the output files on disk.
    It has a method to open a file and automatically closes already opened files
    when the program exits gracefully.

    Since this class has to perform some logic on the patent before writing it
    to the file, such as determining te_global and chem_global, it implements
    its own buffering.\n
    Single lines  are first stored and from these the claims are build.
    Only when the write_patent function is called the class writes to disk.

    This class was rewritten Jan 2020 to work with the csv library
    since my old code was unable to work with '"' in the text.
    I've not touched the buffer claim function since that function
    is somewhat extremely unreadable and needs to be rewritten!

    :param basepath: The path where the output file structure should be placed.
    :type basepath: str, optional
    :param encoding: The encoding for the output files. The default of utf-8
        should be a good choice for most cases.
    :type encoding: str, optional
    """

    def __init__(self, basepath='./', encoding='utf-8'):
        self.basepath = pathlib.Path(basepath)
        self.coding = encoding
        self._file = None
        self._writer = None
        self.claim_buffer = []
        self._buffer = []
        self.te_global = False
        self.chem_global = False

    def new_file(self, year, path):
        """
        This method closes any open files and then opens a new one.
        The name of the file opened is given as path.

        If there is a error e.g. non-existing paths the program will not log
        any errors, but crash since there is no way to recover from not being
        able to write output.

        Right now the method can create subdirectories based if the path
        argument uses a subdirectory.

        :param path: The filename of the output file to be created.
            This filepath should be relative to the basepath given above.
        :type path: str
        :rtype: None
        """
        if self._file is not None:
            self._file.close()
        self._file_path = self.basepath / year / path
        if not self._file_path.parent.is_dir():
            self._file_path.parent.mkdir()

        self._file = open(self._file_path, 'w', encoding=self.coding, newline='')
        self._writer = csv.writer(self._file)

    def check_invalid_references(self):
        """
        When a claim was assigned an invalid dependency (e.g. because the claim was cancelled),
        but is referred by another claim, I also assign an invalid reference to the referring claim.
        This invalidity is transitive, when a claim references an invalid claim, it becomes reason for invalidity itself.
        This is done, since in a patent application claims should not depend on cancelled claims.
        """
        invalid_sequences = [line[3] for line in self._buffer if line[5] < 0]
        if len(invalid_sequences) == 0:
            return
        for line in self._buffer:
            if line[5] in invalid_sequences:
                line[5] = -1
                invalid_sequences.append(line[3])

    def add_line(
            self, sequence, claim_id, level, dependency, text, TE, chem):
        """
        This method adds a line to the line buffer that will be later
        written to disk.

        :param sequence: The parsed sequence of the line.
        :type sequence: int
        :param claim_id: The id number of the claim.
        :type claim_id: int
        :param level: The level of the claim line.
        :type level: int
        :param dependency: The dependency of the claim line as returned by
            the function :func:`~src.rules.apply_rules`.
        :type dependency: int
        :param text: The text of the claims line. This should be all lowercase.
        :type text: str
        :param te: Whether a line contains a table or equation
        :type te: bool
        :param chem: Whether the line contains a chemical formula.
        :type chem: bool
        :rtype: None
        """
        self.claim_buffer.append(
                (TE, chem, claim_id, sequence, level, dependency, text))
        self.te_global = self.te_global or TE
        self.chem_global = self.chem_global or chem

    def write_patent(self, patent_id):
        """
        This method writes the entire patent to disk.
        It should be called if every claim has been buffered.

        :param patent_id: The patent number.
        :type patent_id: str
        :rtype: None
        """
        self.check_invalid_references()
        self._writer.writerows(
                [[patent_id, x2, self.te_global, x3, self.chem_global,
                x4, x5, x6, x7, x8] for
                    (x2, x3, x4, x5, x6, x7, x8) in self._buffer])
        self._buffer = []
        self.te_global = False
        self.chem_global = False

    def buffer_claim(self):
        """
        This function takes a single claim and adds it to the buffer.
        Unfortunately this code is highly unreadable.

        :rtype: None
        """
        # empty claim. Should never happen
        if len(self.claim_buffer) == 0:
            return
        # first line is level != 0, should never happen.
        if self.claim_buffer[0][4] != 1:
            self.claim_buffer = []
            return
        base_index = len(self._buffer)
        self._buffer.append(list(self.claim_buffer[0]))
        # skipped references allows me to look up the skipped lines and
        # what those should refer to
        skipped_references = {}
        for i in range(1, len(self.claim_buffer)):
            # the is statement writes all normal claims and continues
            if self.claim_buffer[i][4] != 1 and self.claim_buffer[i][4] != 7:
                self._buffer.append(list(self.claim_buffer[i]))
                if self.claim_buffer[i][5] in skipped_references:
                    self._buffer[-1][5] = skipped_references[
                            str(self.claim_buffer[i][5])]
                continue

            # I want to write all level 1 lines in one line.
            # while doing so I want to avoid any references to skipped lines.
            self._buffer[base_index][6] = self._buffer[base_index][6] +\
                    ' [[{}]] '.format(self.claim_buffer[i][3])\
                    + self.claim_buffer[i][6]
            # add this line to the skipped_references
            skipped_references[str(self.claim_buffer[i][3])] = self._buffer[base_index][3]
            # if I merge lines and one is TE or CHEM
            # I want to write that to this line
            if self.claim_buffer[i][0] is True:
                self._buffer[base_index][0] = True
            if self.claim_buffer[i][1] is True:
                self._buffer[base_index][1] = True
            if self.claim_buffer[i][4] == 1 and \
                    self._buffer[base_index][5] < self.claim_buffer[i][5]:
                self._buffer[base_index][5] = self.claim_buffer[i][5]
        self.claim_buffer = []

    def __del__(self):
        if self._file is not None:
            self._file.close()
