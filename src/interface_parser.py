"""
This module is supposed to provide a common interface for all data versions -
both xml and text. Therefore it is called from the main and then in turn calles the
actual parsers.
"""

from src import writer
from src import parse_text
from src import parse_xml

import logging

version_headers = {
    "PATFT": "*** BRS DOCUMENT BOUNDARY ***",
    "APS": "PATN",
    "1": 'SYSTEM "pap-v16-2002-01-01.dtd"',
    "25": '"ST32-US-Grant-025xml.dtd"',
    "40": 'SYSTEM "us-patent-grant-v40',
    "41": 'SYSTEM "us-patent-grant-v41',
    "42": 'SYSTEM "us-patent-grant-v42',
    "43": 'SYSTEM "us-patent-grant-v43',
    "44": 'SYSTEM "us-patent-grant-v44',
    "45": 'SYSTEM "us-patent-grant-v45',
}
"""
This This are used to allow me to call different parsers for
different years. Every XML file contains a header that is used to
determine the version.
For the txt files I need the PATN string since there is no header.
"""

version_calls = {
    "PATFT": parse_text.ParsePATFT,
    "APS": parse_text.ParseAPS,
    "1": parse_xml.ParseV1,
    "25": parse_xml.ParseV2,
    "40": parse_xml.ParseV4,
    "41": parse_xml.ParseV4,
    "42": parse_xml.ParseV4,
    "43": parse_xml.ParseV4,
    "44": parse_xml.ParseV4,
    "45": parse_xml.ParseV4,
}
"""
This dict stores the calls to build the parsers. Every version
from the dict above should have a call in this dict with the same key.
"""


class Parser:
    """
    This classed is used to provide a common interface for all the different
    classes used to parse different versions of the xml / txt data.

    It provides only one function to parse the zipname and a constructor.

    :param out_path: Since this class builds the writer object it requires the path
        where the output files should be placed
    :type out_path: str
    """

    parser = None

    def __init__(self, out_path="./", application=False):
        self.writer = writer.Writer(basepath=out_path)
        self.logger = logging.getLogger("uspto.parse")
        self.application = application

    def parse(
        self, year, zip_name, link, historic: bool = False, canadian: bool = False
    ):
        """
        This method is the main entry point for parsing a file.

        It is called with both the filename of the zip file which is then
        passed onto the writer that opens an output file and
        the full path of the input file, which is read in.

        This class first initialized the output file and then reads in the
        first line to determine the version.
        From this the appropriate parser is initialized.

        After this the file is read in and the split method of the parser is
        used and then the method iterates through the patents and parses them.

        :param year: The year being parsed.
        :type year: str
        :param zip_name: Filename of the zipfile from which the
            output filename is derived.
        :type zip_name: str
        :param link: The path of the input file
        :type link: str
        :rtype: None
        """
        self.parser = None
        if historic:
            self.parser = parse_xml.ParseHistoric(self.writer)
        elif canadian:
            self.parser = parse_xml.ParseCanadian(self.writer)
        else:
            with open(link, "r") as ifile:
                next(ifile)
                head = ifile.readline()
                # assumes, that the second line holds metadata
                # If I work on applications, I need to replace 'Application' and 'application' by 'Grant' and 'grant'
                # For the version headers to work.
                if self.application:
                    head = head.replace("Application", "Grant").replace(
                        "application", "grant"
                    )
            for (version, header) in version_headers.items():
                if header in head:
                    self.parser = version_calls[version](self.writer, self.application)
                    self.logger.info("set parser to version: " + version)
        if self.parser is None:
            # A special case for the PATFT files, as these have only one header line.
            if int(year) < 1976:
                self.parser = version_calls["PATFT"](self.writer, self.application)
            else:
                self.logger.critical("no compatible parser found! Check year and code!")
                return None

        if not self.parser.open(link):
            return None

        self.writer.new_file(year, zip_name.name.replace(".zip", ".csv"))

        for (claims, pat_nbr) in self.parser:
            if pat_nbr is None:
                continue
            if pat_nbr[0] in ["D", "H", "T", "P"]:
                continue
            try:
                self.parser.parse_claims(claims, pat_nbr)
            except Exception as err:
                # For now I write patents that fail to the output.
                self.writer.write_patent(pat_nbr)
                self.logger.exception(
                    "Parsing failed, likely bad patent", exc_info=err, pat_nbr=pat_nbr
                )
