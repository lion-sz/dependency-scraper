"""
This module parses the command line flags and the config file.
"""

import argparse
import configparser

DEFAULTS = {
        'start_year': 1976, 'end_year': 2020,
        'path': './', 'out_path': './', 'tmp_path': None}
"""
This Dictionary stores the default values for all non-boolean arguments.
Therefore, alterations to these defaults should be done in this dictionary,
instead of the method.

Both the command line arguments and the entries in the config file take
precedence over these defaults.
"""


def build_parser():
    """
    This function is required to make the sphinx happy.
    It builds the argument parser and adds all the arguments.

    It return the parser object.
    """
    parser = argparse.ArgumentParser(
            description='This script extracts the claim structure from patent grants.')
    parser.add_argument(
            '--applications', action='store_true',
            help="""When set the script works on patent applications instead of grants."""
    )
    parser.add_argument(
            '--year', '-y', type=int, default=None,
            help="""When specified the script is run only for the year provided.\
            If not the script runs for the years specified through the config file.""")
    parser.add_argument(
            '--path', '-p', type=str, default=None,
            help='The path where the patent data files are stored.')
    parser.add_argument(
            '--out_path', '-o', type=str, default=None,
            help='The path where the output files should be placed.\
            Since many small chunks are written to this directory, this should be a local drive.')
    parser.add_argument(
            '--tmp_path', '-t', type=str, default=None,
            help="""A temporary path used for the extracted output files.\
                    If not set the zip files are extracted to the datapath and deleted afterwards.\
                    This option is especially useful if the data is on a potentially slow network drive.""")
    parser.add_argument(
            '--start_year', type=int, default=None,
            help='When several years should be parsed, this option specifies the first year.')
    parser.add_argument(
            '--end_year', type=int, default=None,
            help='When several years should be parsed, this option specifies the last year.')
    parser.add_argument('--quiet', '-q', action='store_true',
            help="""If set this options suppresses all output. More output is written to the logfile.""")
    parser.add_argument(
            '--no_expand', '-e', action='store_true',
            help="""By default the script assumes that the grant data are placed in subfolders,\
                    named after the year for text files and year_xml for the xml files.\
                    If this flag is set the script uses a flat folder structure.""")
    parser.add_argument(
            '--no_offline', action='store_true',
            help="""When this flag is set the script does not try to use offline files and therefore\
                    downloads all files.\
                    If this option is used with an non-empty data path, already existing files might be deleted.""")
    parser.add_argument(
            '--update', '-u', action='store_true',
            help="""Use this flag to only update the existing files.\
                    If set files that are already downloaded are skipped.\
                    When the year is set, the script updates the files for the year specified.\
                    If no year is given the script runs on the current year.""")
    parser.add_argument('--historic', type=str,
                        help='If set the script runs on historic patents, in the csv named in this argument.')
    parser.add_argument("--canada", type=str,
                        help="If set, run with the self-parsed canadian patents in this argument.")
    parser.add_argument(
            '--store', action='store_true',
            help='If set the current configuration values are stored in the file \'uspto.ini\'.')

    return parser


class Config_Reader:
    """
    This class parses the command line and the config file arguments.

    It first reads in the arguments from both command line and the config file
    and then compares the - the command line arguments takes precedence.

    The defaults are stored in the array above.

    :param conf_path: The path to the config file.
    :type conf_path: str, optional
    :param no_check_path: If this flag is set the script does not ensure that
        every path ends with a slash character.
        This might be helpful if your OS does not use the slash as a directory
        seperator.
    :type no_check_path: bool, optional
    """

    def __init__(self, conf_path='./uspto.ini', no_check_path=False):
        self.conf_path = conf_path
        self._no_check_path = no_check_path
        self.config = None
        self.paths_keys = ['path', 'out_path', 'tmp_path']
        self.flags_keys = ['applications', 'no_expand', 'no_offline', 'quiet', 'update']

    def read_conf(self):
        """
        This method opens the configuration file and returns the
        options in a dictionary.

        Note that the year option can not be specified through the config file,
        though the year min and year max can be specified through the file.

        :return: A dictionary containing the arguments and their value.
        :rtype: Dict
        """

        # the dictionary that is supposed to store the parsed arguments
        parsed = {}

        # get the command line arguments
        parser = build_parser()
        cl_args = vars(parser.parse_args())

        # add the year argument to the output file
        parsed['year'] = cl_args['year']

        # then get the config file
        self.config = configparser.ConfigParser()
        self.config.read(self.conf_path)

        # get the three paths.
        try:
            paths = self.config['paths']
            for path in self.paths_keys:
                parsed[path] = paths.get(path, DEFAULTS[path])
        except KeyError:
            for path in self.paths_keys:
                parsed[path] = DEFAULTS[path]

        # get the booleans. They are encapsulated in a flag section
        try:
            flags = self.config['flags']
            for flag in self.flags_keys:
                parsed[flag] = flags.getboolean(flag, False)
        except KeyError:
            for flag in self.flags_keys:
                parsed[flag] = False

        # and finally get the years
        try:
            years = self.config['years']
            parsed['start_year'] = years.getint(
                    'start', DEFAULTS['start_year'])
            parsed['end_year'] = years.getint('end', DEFAULTS['end_year'])
        except KeyError:
            parsed['start_year'] = DEFAULTS['start_year']
            parsed['end_year'] = DEFAULTS['end_year']

        # and now go through the command line arguments.
        # The command line takes precedence
        for (key, value) in cl_args.items():
            # skip the year or the store flag
            if key in ['year', 'store']:
                continue
            # if the argument is not set, therefor None skip it
            elif value is None:
                continue
            # boolean values overwrite the config only if they are true:
            elif type(value) is bool:
                if value is True:
                    parsed[key] = value
            else:
                # all other overwrite regardless of value
                parsed[key] = value

        # For all paths ensure that the path ends in a slash.
        # This behaviour can be supressed.
        if not self._no_check_path:
            for path in self.paths_keys:
                if parsed[path] is not None and parsed[path][-1] != '/':
                    parsed[path] += '/'

        if cl_args['store'] is True:
            self.store_conf(conf=parsed)

        return parsed

    def store_conf(self, conf):
        """
        This method is called to store the config.
        This requires a complete config, so if an options was not specified
        the default is written to the file.

        It first builds a new ConfigParser object, then opens a file
        and writes the parsers content to file.

        :param conf: The dictionary containing the config keys.
        :type conf: Dict
        :rtype: None
        """

        # init the new ConfigParser object
        output_conf = configparser.ConfigParser()

        # write the three paths
        output_conf['paths'] = {}
        for path in ['path', 'out_path', 'tmp_path']:
            if conf[path] is not None:
                output_conf['paths'][path] = conf[path]

        # and the four flags
        output_conf['flags'] = {}
        for flag in self.flags_keys:
            output_conf['flags'][flag] = str(conf[flag])

        # and finally the two years
        output_conf['years'] = {}
        output_conf['years']['start'] = str(conf['start_year'])
        output_conf['years']['end'] = str(conf['end_year'])

        # and write to file
        with open(self.conf_path, 'w') as conf_file:
            output_conf.write(conf_file)
