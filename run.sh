#!/usr/bin/sh

# the script can onyl run if two years are provided or the update flag is set.
if ! $( [ "$#" == 2 ] || [ "$1" == "update" ] )
then
    echo "Please provide either both a start and end year, or the update flag."
    exit 1
fi

# change the paths here
data_path="/nextcloud/pats/data/"
output_path="/data/lion/pats/out/"
tmp_path="/data/lion/pats/tmp/"
target_path="/nextcloud/pats/claims/"
log_path="../logs/"


# if the update flag is set run a simpler version.
if [ "$1" == "update" ]
then
    python uspto.py -p "$data_path" -o "$output_path" -t "$tmp_path" -u
    exit 0
fi

for year in $(seq $1 $2)
do
    python uspto.py -y "$year" -p "$data_path" -o "$output_path" -t "$tmp_path"
    mv "uspto.log" "$log_path$year".log
    zip -j "$tmp_path$year"".zip" "$output_path$year"/*
    mv "$tmp_path$year"".zip" "$target_path"
    rm -r "$output_path$year"
done
