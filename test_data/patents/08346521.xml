<patent pat_nbr="08346521">
  <claims>
    <claims id="claims">
<claim id="CLM-00001" num="00001">
<claim-text>1. A method of determining the feasibility of a proposed structure analysis process, said process comprising electron beam excitation of x-rays from a multi-layered structure in order to determine the thicknesses and compositions of the layers, the method comprising:
<claim-text>i) generating nominal x-ray data representing the x-ray excitation response of the multi-layered structure according to a first set of one or more process conditions, using nominal structure data comprising compositions and thicknesses for each layer;</claim-text>
<claim-text>ii) analysing whether or not it is possible for a structure solver, when given the nominal x-ray data generated according to the first set of process conditions, to calculate output structure data comprising compositions and thicknesses for each layer; and</claim-text>
<claim-text>iii) automatically repeating steps (i) and (ii) using different process conditions in place of the first set of process conditions so as to determine under which process conditions, if any, the proposed structure analysis process can be used to determine the thickness and composition of each layer in the multi-layered structure.</claim-text>
</claim-text>
</claim>
<claim id="CLM-00002" num="00002">
<claim-text>2. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein the nominal x-ray data are generated using a predictor model as a function of the elemental composition and layer thickness of each layer in the multi-layered structure.</claim-text>
</claim>
<claim id="CLM-00003" num="00003">
<claim-text>3. A method according to <claim-ref idref="CLM-00002">claim 2</claim-ref>, wherein the predictor model is used to generate nominal x-ray data representing the characteristic x-ray intensity response for a particular element within the multi-layered structure.</claim-text>
</claim>
<claim id="CLM-00004" num="00004">
<claim-text>4. A method according to <claim-ref idref="CLM-00002">claim 2</claim-ref>, wherein step (ii) comprises analysing the response of the predictor model to changes in the layer thicknesses and/or compositions within the nominal structure data.</claim-text>
</claim>
<claim id="CLM-00005" num="00005">
<claim-text>5. A method according to <claim-ref idref="CLM-00004">claim 4</claim-ref>, wherein the predictor model is analysed as an approximated series expansion.</claim-text>
</claim>
<claim id="CLM-00006" num="00006">
<claim-text>6. A method according to <claim-ref idref="CLM-00005">claim 5</claim-ref>, wherein the behaviour of the predictor model is represented as a matrix equation, wherein the nominal x-ray data output from the predictor model are represented as a vector of M values, wherein unknown layer thicknesses and/or compositions are represented as a vector of N variables, and wherein the partial derivatives relating the outputs to the layer thicknesses or compositions are represented as an M by N dimension Jacobian matrix.</claim-text>
</claim>
<claim id="CLM-00007" num="00007">
<claim-text>7. A method according to <claim-ref idref="CLM-00006">claim 6</claim-ref>, wherein the analysis is performed using an iterative technique to minimise the resultant residual vector.</claim-text>
</claim>
<claim id="CLM-00008" num="00008">
<claim-text>8. A method according to <claim-ref idref="CLM-00007">claim 7</claim-ref>, further comprising calculating a condition number representing the degree of solubility of the matrix equation for the layer thicknesses or compositions.</claim-text>
</claim>
<claim id="CLM-00009" num="00009">
<claim-text>9. A method according to <claim-ref idref="CLM-00008">claim 8</claim-ref>, wherein the condition number is compared with a threshold comprising a feasibility criterion.</claim-text>
</claim>
<claim id="CLM-00010" num="00010">
<claim-text>10. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein, in step (ii), the nominal structure data is modified and input, together with the nominal x-ray data, into the structure solver adapted to iteratively calculate the composition and thickness of each layer of the multi-layered structure as output structure data using the nominal x-ray data and modified structure data.</claim-text>
</claim>
<claim id="CLM-00011" num="00011">
<claim-text>11. A method according to <claim-ref idref="CLM-00010">claim 10</claim-ref>, wherein, during operation, the structure solver calculates x-ray data for the modified structure data and compares the calculated x-ray data with the nominal x-ray data.</claim-text>
</claim>
<claim id="CLM-00012" num="00012">
<claim-text>12. A method according to <claim-ref idref="CLM-00011">claim 11</claim-ref>, wherein the effects of modifications to the nominal structure data are analysed by comparing the calculated x-ray data with the nominal x-ray data and comparing the results of the comparison with a feasibility criterion.</claim-text>
</claim>
<claim id="CLM-00013" num="00013">
<claim-text>13. A method according to <claim-ref idref="CLM-00012">claim 12</claim-ref>, wherein the feasibility criterion is met if a unique set of output structure data is found which produces corresponding x-ray data that is substantially identical to the nominal x-ray data and is substantially independent of the modifications applied to the nominal structure data.</claim-text>
</claim>
<claim id="CLM-00014" num="00014">
<claim-text>14. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein, in step (ii), the x-ray data is modified and input, together with the nominal structure data, into a structure solver adapted to iteratively calculate the composition and thickness of each layer of the multi-layered structure using the modified x-ray data and nominal structure data, and to produce output structure data.</claim-text>
</claim>
<claim id="CLM-00015" num="00015">
<claim-text>15. A method according to <claim-ref idref="CLM-00014">claim 14</claim-ref>, wherein the effects of modifications to the x-ray data are analysed by comparing the output structure data with the nominal structure data in accordance with a feasibility criterion.</claim-text>
</claim>
<claim id="CLM-00016" num="00016">
<claim-text>16. A method according to <claim-ref idref="CLM-00015">claim 15</claim-ref>, wherein the feasibility criterion is met if, for every modification to the nominal x-ray data, the structure solver produces a unique set of output structure data having corresponding x-ray data that is substantially identical to the nominal x-ray data.</claim-text>
</claim>
<claim id="CLM-00017" num="00017">
<claim-text>17. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein, using a Monte Carlo method, changes to nominal x-ray data generated from nominal structure data are produced by applying statistical distributions to the nominal x-ray data so as to produce a plurality of sets of modified x-ray data having a collective statistical distribution equivalent to that expected from performing the x-ray excitation experimentally.</claim-text>
</claim>
<claim id="CLM-00018" num="00018">
<claim-text>18. A method according to <claim-ref idref="CLM-00017">claim 17</claim-ref>, wherein in step (ii) each set of modified x-ray data is entered, together with the nominal structure data into an iterative structure solver for generating output structure data.</claim-text>
</claim>
<claim id="CLM-00019" num="00019">
<claim-text>19. A method according to <claim-ref idref="CLM-00018">claim 18</claim-ref>, wherein the process is deemed feasible if, for every modification to the nominal x-ray data, the structure solver produces a unique set of output structure data having corresponding x-ray data that is substantially identical to the nominal x-ray data.</claim-text>
</claim>
<claim id="CLM-00020" num="00020">
<claim-text>20. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein step (i) comprises generating one or more sets of predicted spectral data, each set representing the x-ray excitation response of the structure according to a respective one of the said process conditions and wherein step (ii) comprises calculating output structure data for each set of process conditions, using each generated set of spectral data and nominal structure data.</claim-text>
</claim>
<claim id="CLM-00021" num="00021">
<claim-text>21. A method according to <claim-ref idref="CLM-00020">claim 20</claim-ref>, further comprising, for each set of process conditions, repeating step (i) a number of times so as to simulate the effects of Poisson counting statistics within each spectral data set by adding random numbers to the spectral data.</claim-text>
</claim>
<claim id="CLM-00022" num="00022">
<claim-text>22. A method according to <claim-ref idref="CLM-00020">claim 20</claim-ref>, wherein step (i) further comprises predicting a statistical distribution of x-ray peak areas due to Poisson counting statistics; and applying the statistical distribution to the spectral data.</claim-text>
</claim>
<claim id="CLM-00023" num="00023">
<claim-text>23. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, further comprising identifying sources of error within the analysis and assigning a figure of merit to the sensitivity of the analysis process to each source of error.</claim-text>
</claim>
<claim id="CLM-00024" num="00024">
<claim-text>24. A method according to <claim-ref idref="CLM-00023">claim 23</claim-ref>, further comprising repeating the method for each of a number of different process conditions in step (iii) and selecting a set of process conditions in which the figure of merit is minimised or is below a predetermined threshold.</claim-text>
</claim>
<claim id="CLM-00025" num="00025">
<claim-text>25. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein each process condition comprises a different electron beam energy and/or a different detector geometry.</claim-text>
</claim>
<claim id="CLM-00026" num="00026">
<claim-text>26. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein sets of x-ray data are generated for different processing conditions which represent fluctuations in the electron beam current.</claim-text>
</claim>
<claim id="CLM-00027" num="00027">
<claim-text>27. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein the process conditions represent process variations associated with the use of predetermined apparatus for performing the electron beam excitation.</claim-text>
</claim>
<claim id="CLM-00028" num="00028">
<claim-text>28. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein the nominal x-ray data are in the form of k-ratios.</claim-text>
</claim>
<claim id="CLM-00029" num="00029">
<claim-text>29. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein initial values for the compositions and thicknesses of the layers in the nominal structure data are selected based upon known values or user inputted values.</claim-text>
</claim>
<claim id="CLM-00030" num="00030">
<claim-text>30. A method according to <claim-ref idref="CLM-00029">claim 29</claim-ref>, where a number of values for the composition and/or structure of the layers are fixed as a constant during the method.</claim-text>
</claim>
<claim id="CLM-00031" num="00031">
<claim-text>31. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, further comprises calculating a standard deviation for one or more values of the output structure data as a measure of achievable precision.</claim-text>
</claim>
<claim id="CLM-00032" num="00032">
<claim-text>32. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein x-ray data are generated so as to represent data produced by an energy dispersive (EDS) or wavelength dispersive (WDS) system.</claim-text>
</claim>
<claim id="CLM-00033" num="00033">
<claim-text>33. A method according to <claim-ref idref="CLM-00001">claim 1</claim-ref>, wherein step (i) is performed using a computer software model of a physical electron beam x-ray analysis system.</claim-text>
</claim>
<claim id="CLM-00034" num="00034">
<claim-text>34. A non-transitory computer program product comprising computer program code means adapted to perform the method of any of the preceding claims, when executed upon a computer.</claim-text>
</claim>
<claim id="CLM-00035" num="00035">
<claim-text>35. A computer program product according to <claim-ref idref="CLM-00034">claim 34</claim-ref>, embodied upon a non-transitory computer readable medium. </claim-text>
</claim>
</claims>
  </claims>
  <results>
    <line cid="1" seq="1" lvl="1" dep="0">1. a method of determining the feasibility of a proposed structure analysis process, said process comprising electron beam excitation of x-rays from a multi-layered structure in order to determine the thicknesses and compositions of the layers, the method comprising:</line>
    <line cid="1" seq="2" lvl="2" dep="1">i) generating nominal x-ray data representing the x-ray excitation response of the multi-layered structure according to a first set of one or more process conditions, using nominal structure data comprising compositions and thicknesses for each layer;</line>
    <line cid="1" seq="3" lvl="2" dep="1">ii) analysing whether or not it is possible for a structure solver, when given the nominal x-ray data generated according to the first set of process conditions, to calculate output structure data comprising compositions and thicknesses for each layer; and</line>
    <line cid="1" seq="4" lvl="2" dep="1">iii) automatically repeating steps (i) and (ii) using different process conditions in place of the first set of process conditions so as to determine under which process conditions, if any, the proposed structure analysis process can be used to determine the thickness and composition of each layer in the multi-layered structure.</line>
    <line cid="2" seq="5" lvl="1" dep="1">2. a method according to claim 1, wherein the nominal x-ray data are generated using a predictor model as a function of the elemental composition and layer thickness of each layer in the multi-layered structure.</line>
    <line cid="3" seq="6" lvl="1" dep="5">3. a method according to claim 2, wherein the predictor model is used to generate nominal x-ray data representing the characteristic x-ray intensity response for a particular element within the multi-layered structure.</line>
    <line cid="4" seq="7" lvl="1" dep="5">4. a method according to claim 2, wherein step (ii) comprises analysing the response of the predictor model to changes in the layer thicknesses and/or compositions within the nominal structure data.</line>
    <line cid="5" seq="8" lvl="1" dep="7">5. a method according to claim 4, wherein the predictor model is analysed as an approximated series expansion.</line>
    <line cid="6" seq="9" lvl="1" dep="8">6. a method according to claim 5, wherein the behaviour of the predictor model is represented as a matrix equation, wherein the nominal x-ray data output from the predictor model are represented as a vector of m values, wherein unknown layer thicknesses and/or compositions are represented as a vector of n variables, and wherein the partial derivatives relating the outputs to the layer thicknesses or compositions are represented as an m by n dimension jacobian matrix.</line>
    <line cid="7" seq="10" lvl="1" dep="9">7. a method according to claim 6, wherein the analysis is performed using an iterative technique to minimise the resultant residual vector.</line>
    <line cid="8" seq="11" lvl="1" dep="10">8. a method according to claim 7, further comprising calculating a condition number representing the degree of solubility of the matrix equation for the layer thicknesses or compositions.</line>
    <line cid="9" seq="12" lvl="1" dep="11">9. a method according to claim 8, wherein the condition number is compared with a threshold comprising a feasibility criterion.</line>
    <line cid="10" seq="13" lvl="1" dep="1">10. a method according to claim 1, wherein, in step (ii), the nominal structure data is modified and input, together with the nominal x-ray data, into the structure solver adapted to iteratively calculate the composition and thickness of each layer of the multi-layered structure as output structure data using the nominal x-ray data and modified structure data.</line>
    <line cid="11" seq="14" lvl="1" dep="13">11. a method according to claim 10, wherein, during operation, the structure solver calculates x-ray data for the modified structure data and compares the calculated x-ray data with the nominal x-ray data.</line>
    <line cid="12" seq="15" lvl="1" dep="14">12. a method according to claim 11, wherein the effects of modifications to the nominal structure data are analysed by comparing the calculated x-ray data with the nominal x-ray data and comparing the results of the comparison with a feasibility criterion.</line>
    <line cid="13" seq="16" lvl="1" dep="15">13. a method according to claim 12, wherein the feasibility criterion is met if a unique set of output structure data is found which produces corresponding x-ray data that is substantially identical to the nominal x-ray data and is substantially independent of the modifications applied to the nominal structure data.</line>
    <line cid="14" seq="17" lvl="1" dep="1">14. a method according to claim 1, wherein, in step (ii), the x-ray data is modified and input, together with the nominal structure data, into a structure solver adapted to iteratively calculate the composition and thickness of each layer of the multi-layered structure using the modified x-ray data and nominal structure data, and to produce output structure data.</line>
    <line cid="15" seq="18" lvl="1" dep="17">15. a method according to claim 14, wherein the effects of modifications to the x-ray data are analysed by comparing the output structure data with the nominal structure data in accordance with a feasibility criterion.</line>
    <line cid="16" seq="19" lvl="1" dep="18">16. a method according to claim 15, wherein the feasibility criterion is met if, for every modification to the nominal x-ray data, the structure solver produces a unique set of output structure data having corresponding x-ray data that is substantially identical to the nominal x-ray data.</line>
    <line cid="17" seq="20" lvl="1" dep="1">17. a method according to claim 1, wherein, using a monte carlo method, changes to nominal x-ray data generated from nominal structure data are produced by applying statistical distributions to the nominal x-ray data so as to produce a plurality of sets of modified x-ray data having a collective statistical distribution equivalent to that expected from performing the x-ray excitation experimentally.</line>
    <line cid="18" seq="21" lvl="1" dep="20">18. a method according to claim 17, wherein in step (ii) each set of modified x-ray data is entered, together with the nominal structure data into an iterative structure solver for generating output structure data.</line>
    <line cid="19" seq="22" lvl="1" dep="21">19. a method according to claim 18, wherein the process is deemed feasible if, for every modification to the nominal x-ray data, the structure solver produces a unique set of output structure data having corresponding x-ray data that is substantially identical to the nominal x-ray data.</line>
    <line cid="20" seq="23" lvl="1" dep="1">20. a method according to claim 1, wherein step (i) comprises generating one or more sets of predicted spectral data, each set representing the x-ray excitation response of the structure according to a respective one of the said process conditions and wherein step (ii) comprises calculating output structure data for each set of process conditions, using each generated set of spectral data and nominal structure data.</line>
    <line cid="21" seq="24" lvl="1" dep="23">21. a method according to claim 20, further comprising, for each set of process conditions, repeating step (i) a number of times so as to simulate the effects of poisson counting statistics within each spectral data set by adding random numbers to the spectral data.</line>
    <line cid="22" seq="25" lvl="1" dep="23">22. a method according to claim 20, wherein step (i) further comprises predicting a statistical distribution of x-ray peak areas due to poisson counting statistics; and applying the statistical distribution to the spectral data.</line>
    <line cid="23" seq="26" lvl="1" dep="1">23. a method according to claim 1, further comprising identifying sources of error within the analysis and assigning a figure of merit to the sensitivity of the analysis process to each source of error.</line>
    <line cid="24" seq="27" lvl="1" dep="26">24. a method according to claim 23, further comprising repeating the method for each of a number of different process conditions in step (iii) and selecting a set of process conditions in which the figure of merit is minimised or is below a predetermined threshold.</line>
    <line cid="25" seq="28" lvl="1" dep="1">25. a method according to claim 1, wherein each process condition comprises a different electron beam energy and/or a different detector geometry.</line>
    <line cid="26" seq="29" lvl="1" dep="1">26. a method according to claim 1, wherein sets of x-ray data are generated for different processing conditions which represent fluctuations in the electron beam current.</line>
    <line cid="27" seq="30" lvl="1" dep="1">27. a method according to claim 1, wherein the process conditions represent process variations associated with the use of predetermined apparatus for performing the electron beam excitation.</line>
    <line cid="28" seq="31" lvl="1" dep="1">28. a method according to claim 1, wherein the nominal x-ray data are in the form of k-ratios.</line>
    <line cid="29" seq="32" lvl="1" dep="1">29. a method according to claim 1, wherein initial values for the compositions and thicknesses of the layers in the nominal structure data are selected based upon known values or user inputted values.</line>
    <line cid="30" seq="33" lvl="1" dep="32">30. a method according to claim 29, where a number of values for the composition and/or structure of the layers are fixed as a constant during the method.</line>
    <line cid="31" seq="34" lvl="1" dep="1">31. a method according to claim 1, further comprises calculating a standard deviation for one or more values of the output structure data as a measure of achievable precision.</line>
    <line cid="32" seq="35" lvl="1" dep="1">32. a method according to claim 1, wherein x-ray data are generated so as to represent data produced by an energy dispersive (eds) or wavelength dispersive (wds) system.</line>
    <line cid="33" seq="36" lvl="1" dep="1">33. a method according to claim 1, wherein step (i) is performed using a computer software model of a physical electron beam x-ray analysis system.</line>
    <line cid="34" seq="37" lvl="1" dep="36">34. a non-transitory computer program product comprising computer program code means adapted to perform the method of any of the preceding claims, when executed upon a computer.</line>
    <line cid="35" seq="38" lvl="1" dep="37">35. a computer program product according to claim 34, embodied upon a non-transitory computer readable medium.</line>
  </results>
</patent>
